/*
 * AUTHOR: Jay Schmidgall <jay@spdbump.sungardss.com>
 *
 * $Id: list3.c,v 1.8 2006/05/15 21:14:13 tobiasoed Exp $
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif

#include <stdio.h>
#include <stdlib.h>
#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif
#include <Xm/Form.h>
#include <Xm/Label.h>
#include <Xm/TextF.h>
#include <Xm/PushB.h>
#include <Xbae/Matrix.h>

static String fallback[] = {
    "List*List*background:        gray",
    "List*mw.fill:                True",
    "List*mw.oddRowBackground:    CadetBlue",
    "List*mw.evenRowBackground:   ForestGreen",
    "List*mw.highlightColor:      Blue",
    "List*mw.reverseSelect:       True",
    "List*mw.gridType:            GRID_ROW_SHADOW",
    "List*mw.cellShadowThickness: 0",
    "List*mw.rows:                10",
    "List*mw.columns:             4",
    "List*mw.visibleRows:         5",
    "List*mw.columnWidths:        10, 10, 10, 10, 10, 5,"
    "                             10, 5, 10, 5, 10, 5",
    "List*mw.columnLabels:        Zero, One, Two, Three, Four,"
    "                             Five, Six, Seven, Eight, Nine",
    "List*mw.rowLabels:           0, 1, 2, 3, 4, 5, 6, 7, 8, 9",
    "List*mw.fill:                True",
    "List*mw.horzFill:            True",
    "List*mw.traversalOn:         True",
	"List*mw.textTranslations:	"
    "                           <Key>osfUp:       EditCell(Up)\\n"
	"                           <Key>osfDown:     EditCell(Down)\\n"
	"                           <Key>osfPageUp:   PageUp()\\n"
	"                           <Key>osfPageDown: PageDown()\\n"
	"                           Ctrl<Key>space:   SelectCell(Toggle)\\n"
	"                           <Key>space:       SelectCell(This)\\n",
	"List*mw.translations:	"
	"                           Ctrl<Btn1Down>:   EditCell(Pointer) SelectCell(Toggle)\\n"
    "                           <Btn1Up>:         DefaultAction()\\n"
	"                           <Btn1Down>:       DefaultAction() EditCell(Pointer) SelectCell(PointerExtend)\\n"
    "                           <Btn1Motion>:     HandleTracking()\n",
    NULL
};

void
LoadMatrix(Widget w)
{
    String *cells[10];
    static String rows[10][10] = {
    { "0,Zero", "0,One",  "0,Two",    "0,Three",  "0,Four",
      "0,Five", "0,Six",  "0, Seven", "0, Eight", "0, Nine" },
    { "1,Zero", "1,One",  "1,Two",    "1,Three",  "1,Four",
      "1,Five", "1,Six",  "1, Seven", "1, Eight", "1, Nine" },
    { "2,Zero", "2,One",  "2,Two",    "2,Three",  "2,Four",
      "2,Five", "2,Six",  "2, Seven", "2, Eight", "2, Nine" },
    { "3,Zero", "3,One",  "3,Two",    "3,Three",  "3,Four",
      "3,Five", "3,Six",  "3, Seven", "3, Eight", "3, Nine" },
    { "4,Zero", "4,One",  "4,Two",    "4,Three",  "4,Four",
      "4,Five", "4,Six",  "4, Seven", "4, Eight", "4, Nine" },
    { "5,Zero", "5,One",  "5,Two",    "5,Three",  "5,Four",
      "5,Five", "5,Six",  "5, Seven", "5, Eight", "5, Nine" },
    { "6,Zero", "6,One",  "6,Two",    "6,Three",  "6,Four",
      "6,Five", "6,Six",  "6, Seven", "6, Eight", "6, Nine" },
    { "7,Zero", "7,One",  "7,Two",    "7,Three",  "7,Four",
      "7,Five", "7,Six",  "7, Seven", "7, Eight", "7, Nine" },
    { "8,Zero", "8,One",  "8,Two",    "8,Three",  "8,Four",
      "8,Five", "8,Six",  "8, Seven", "8, Eight", "8, Nine" },
    { "9,Zero", "9,One",  "9,Two",    "9,Three",  "9,Four",
      "9,Five", "9,Six",  "9, Seven", "9, Eight", "9, Nine" }
    };

    cells[0] = &rows[0][0];
    cells[1] = &rows[1][0];
    cells[2] = &rows[2][0];
    cells[3] = &rows[3][0];
    cells[4] = &rows[4][0];
    cells[5] = &rows[5][0];
    cells[6] = &rows[6][0];
    cells[7] = &rows[7][0];
    cells[8] = &rows[8][0];
    cells[9] = &rows[9][0];
    
    XtVaSetValues(w,
          XmNcells,     cells,
          NULL);
}

void 
cbEnterCell(Widget w, XtPointer client, XtPointer call)
{
    XbaeMatrixEnterCellCallbackStruct *cbs = call;
    cbs->doit = False;
    cbs->map = False;
}

void
cbSelect(Widget w, XtPointer client, XtPointer call)
{
    XbaeMatrixSelectCellCallbackStruct *cbs = call;
    int i;
    
    for(i=0;i<cbs->num_params;i++){
        printf("|%s| ",cbs->params[i]);
    }
    printf("\n");

    if(strcmp(cbs->params[0],"This")==0 || strcmp(cbs->params[0], "PointerExtend")==0) {
        XbaeMatrixDeselectAll(w);
        XbaeMatrixSelectRow(w, cbs->row);
    } else if(strcmp(cbs->params[0],"Toggle")==0) {
        if (XbaeMatrixIsRowSelected(w, cbs->row)) {
            XbaeMatrixDeselectRow(w, cbs->row);
        } else {
            XbaeMatrixSelectRow(w, cbs->row);
        }
    } else if(strcmp(cbs->params[0],"Extend")==0) {
            XbaeMatrixSelectRow(w, cbs->row);
    }
}

void 
cbTraverse(Widget w, XtPointer client, XtPointer call)
{
    XbaeMatrixTraverseCellCallbackStruct *cbs =
        (XbaeMatrixTraverseCellCallbackStruct *) call;

    if (strcmp(cbs->param, "LosingFocus") == 0) {
        XbaeMatrixUnhighlightRow(w, cbs->row);
    } else if (strcmp(cbs->param, "Focus") == 0 || cbs->row != cbs->next_row) {
        /*
         * When we get the focus for the first time, there is no current row
         */
        if (cbs->row != -1) {
            XbaeMatrixUnhighlightRow(w, cbs->row);
        }

        XbaeMatrixHighlightRow(w, cbs->next_row);
    }
}

Widget toplevel, form, label, mw, b;
Widget    tf1, tf2;

void resizeRow(Widget w, XtPointer client, XtPointer call)
{
    int    row, height;

    row = atoi(XmTextFieldGetString(tf1));
    height = atoi(XmTextFieldGetString(tf2));

    fprintf(stderr, "Setting height of row %d to %d\n", row, height);

    XbaeMatrixSetRowHeight(mw, row, height);
}

int
main(int argc, char *argv[])
{
    XtAppContext app;
    char *name =
        "The matrix mimics the keyboard interaction\n"
        "of XmList. Up and Down move the highlight up and down\n"
        "while Space and Doubble click toggle the selection.\n"
        "\n"
        "Use the left textfield to specify a row, the right one to\n"
        "specify its new height, and click the button to set the new height\n";
    XmString xms;

    toplevel = XtVaAppInitialize(&app, "List",
                    NULL, 0,
                    &argc, argv,
                    fallback,
                    NULL);
#ifdef USE_EDITRES
    XtAddEventHandler( toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
#endif

    form = XtVaCreateManagedWidget("form", xmFormWidgetClass, toplevel,
                    NULL);

    xms = XmStringCreateLtoR(name, XmFONTLIST_DEFAULT_TAG);
    label = XtVaCreateManagedWidget("label", xmLabelWidgetClass, form,
                    XmNlabelString, xms,
                    XmNtopAttachment, XmATTACH_FORM,
                    XmNtopOffset, 4,
                    XmNleftAttachment, XmATTACH_FORM,
                    XmNleftOffset, 4,
                    XmNrightAttachment, XmATTACH_FORM,
                    XmNrightOffset, 4,
                    NULL);
    XmStringFree(xms);

    tf1 = XtVaCreateManagedWidget("tf1", xmTextFieldWidgetClass, form,
                    XmNtopOffset, 4,
                    XmNleftAttachment, XmATTACH_FORM,
                    XmNleftOffset, 4,
                    XmNbottomAttachment, XmATTACH_FORM,
                    XmNbottomOffset, 4,
                    NULL);

    tf2 = XtVaCreateManagedWidget("tf2", xmTextFieldWidgetClass, form,
                    XmNtopOffset, 4,
                    XmNleftAttachment, XmATTACH_WIDGET,
                    XmNleftWidget, tf1,
                    XmNleftOffset, 4,
                    XmNbottomAttachment, XmATTACH_FORM,
                    XmNbottomOffset, 4,
                    NULL);

    b = XtVaCreateManagedWidget("Push Me", xmPushButtonWidgetClass, form,
                    XmNtopOffset, 4,
                    XmNleftAttachment, XmATTACH_WIDGET,
                    XmNleftWidget, tf2,
                    XmNleftOffset, 4,
                    XmNbottomAttachment, XmATTACH_FORM,
                    XmNbottomOffset, 4,
                    XmNrightAttachment, XmATTACH_FORM,
                    XmNrightOffset, 4,
                    NULL);

    XtAddCallback(b, XmNactivateCallback, resizeRow, 0);
   
    mw = XtVaCreateManagedWidget("mw", xbaeMatrixWidgetClass, form,
                    XmNtopWidget, label,
                    XmNtopAttachment, XmATTACH_WIDGET,
                    XmNtopOffset, 4,
                    XmNleftAttachment, XmATTACH_FORM,
                    XmNleftOffset, 4,
                    XmNbottomWidget, b,
                    XmNbottomAttachment, XmATTACH_WIDGET,
                    XmNbottomOffset, 4,
                    XmNrightAttachment, XmATTACH_FORM,
                    XmNrightOffset, 4,
                    XmNselectionPolicy, XmEXTENDED_SELECT,
                    NULL);

    XtAddCallback(mw, XmNselectCellCallback, cbSelect, NULL);
    XtAddCallback(mw, XmNenterCellCallback, cbEnterCell, NULL);
    XtAddCallback(mw, XmNtraverseCellCallback, cbTraverse, NULL);
    LoadMatrix(mw);

    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);
    /*NOTREACHED*/
    return 0;
}
