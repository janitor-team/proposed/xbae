/*
 * Copyright(c) 1992 Bell Communications Research, Inc. (Bellcore)
 *                        All rights reserved
 *
 * Copyright � 2001 by the LessTif Developers
 *
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * $Id: select-drag.c,v 1.12 2006/05/16 10:52:46 tobiasoed Exp $
 */


#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif

#include <stdlib.h>
#include <stdio.h>

#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif

#include <Xm/Xm.h>
#include <Xm/Form.h>
#include <Xm/PushB.h>
#include <Xm/PanedW.h>

#include <Xbae/Matrix.h>

void LoadMatrix(Widget matrix);
void SelectCB(Widget w, XtPointer client_data, XbaeMatrixSelectCellCallbackStruct *call_data);
void ResetCB(Widget w, XtPointer cd, XtPointer cb);
void scroll(XtPointer data);

#define N_VISIBLE 10

#define N_FIXED_ROWS 10
#define N_ROWS 40

#define N_FIXED_COLUMNS 10
#define N_COLUMNS 30

#define makestring(N) #N
#define string(N) makestring(N)

#define Min(a,b) ((a < b) ? (a) : (b))
#define Max(a,b) ((a > b) ? (a) : (b))

static String fallback[] = {
        "Select-drag*matrix.rows:                " string(N_ROWS),
        "Select-drag*matrix.columns:             " string(N_COLUMNS),
        "Select-drag*matrix.visibleRows:         " string(N_VISIBLE),
        "Select-drag*matrix.visibleColumns:      " string(N_VISIBLE),
        "Select-drag*matrix.fixedRows:           " string(N_FIXED_ROWS),
        "Select-drag*matrix.fixedColumns:        " string(N_FIXED_ROWS),
        "Select-drag*matrix.gridType:        grid_cell_shadow",
/*
 * We have an uneditable matrix.
 */
        "Select-drag*matrix.traversalOn:        False",
        "Select-drag*matrix.translations:        #override\\n"
        "                        <Btn1Down>: SelectCell(PointerExtend)\\n",
        "Select-drag*matrix.selectionPolicy:        extended_select",
        NULL
};

/*
 * Create an uneditable matrix. When Btn1 is dragged, a rectangular region
 * of cells is selected.
 */

int
main(int argc, char *argv[])
{
    Widget toplevel, matrix, pb, pane;
    XtAppContext app;

    toplevel = XtAppInitialize(
        &app, "Select-drag", NULL, 0, &argc, argv, fallback, NULL, 0);
#ifdef USE_EDITRES
    XtAddEventHandler(toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
#endif

    pane = XtVaCreateManagedWidget(
        "pane", xmPanedWindowWidgetClass, toplevel,
        NULL);

    pb = XtVaCreateManagedWidget(
        "Reset", xmPushButtonWidgetClass, pane,
        NULL);

    matrix = XtVaCreateManagedWidget(
        "matrix", xbaeMatrixWidgetClass, pane,
        XmNtopAttachment, XmATTACH_FORM,
        XmNbottomAttachment, XmATTACH_FORM,
        XmNleftAttachment, XmATTACH_FORM,
        XmNtopAttachment, XmATTACH_FORM,
        XmNselectScrollVisible, False,
        NULL);

    XtAddCallback(matrix, XmNselectCellCallback, (XtCallbackProc)SelectCB, NULL);

    XtAddCallback(pb, XmNactivateCallback, (XtCallbackProc)ResetCB, (XtPointer)matrix);
    
    LoadMatrix(matrix);
    
    #if 0
    scroll((XtPointer) matrix);
    #endif

    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);
    return 0;
}

/*
 * Callback when the mouse is moved, we want to select a rectangle of cells
 */
/* ARGSUSED */
void
SelectCB(Widget w, XtPointer client_data, XbaeMatrixSelectCellCallbackStruct *call_data)
{
    static int start_row = -1, start_column = -1;
    static int last_row = -1, last_column = -1;
    int i, j;

    /*
     * Avoid scrolling all over the place when selecting cells
     */
    /*XtVaSetValues(w, XmNselectScrollVisible, False, NULL);*/

    if (strcmp(call_data->params[0], "PointerExtend") == 0)
    {
        start_row = last_row = call_data->row;
        start_column = last_column = call_data->column;
        XbaeMatrixSelectCell(w, last_row, last_column);
    }
    /*
     * Else we might be in an extended drag operation
     */
    else if (strcmp(call_data->params[ 0 ], "Extend") == 0)
    {
        /*
         * This is a pretty simplistic, stupid implementation
         * of selecting a rectangle.
         */

        for (i = Min(start_row, last_row); i <= Max(start_row, last_row); i++)
            for (j = Min(start_column, last_column); j <= Max(start_column, last_column); j++)
                XbaeMatrixDeselectCell(w, i, j);
        for (i = Min(start_row, call_data->row); i <= Max(start_row, call_data->row); i++)
            for (j = Min(start_column, call_data->column); j <= Max(start_column, call_data->column); j++)
                XbaeMatrixSelectCell(w, i, j);
    
        last_row = call_data->row;
        last_column = call_data->column;
        #if 0
        printf("%d, %d\n", last_row, last_column);
        #endif
    }
}

/* ARGSUSED */
void
LoadMatrix(Widget matrix)
{
    int i, j;
    int rows, columns;
    char buf[BUFSIZ];
    String **cells;

    XtVaGetValues(matrix,
                  XmNrows, &rows,
                  XmNcolumns, &columns,
                  NULL);

    cells = (String **) XtMalloc(sizeof(String *) * rows);
    for (i = 0; i < rows; i++) {
        cells[i] = (String *) XtMalloc(sizeof(String) * columns);
        for (j = 0; j < columns; j++) {
            sprintf(buf, "r%dc%d", i, j);
            cells[i][j] = XtNewString(buf);
        }
    }

    XtVaSetValues(matrix,
                  XmNcells, cells,
                  NULL);

    for (i = 0; i < rows; i++) {
        for (j = 0; j < columns; j++)
            XtFree((XtPointer)cells[i][j]);
        XtFree((XtPointer)cells[i]);
    }
    XtFree((XtPointer)cells);
}

void
ResetCB(Widget w, XtPointer cd, XtPointer cb)
{
    Widget mw = (Widget)cd;

    XbaeMatrixDeselectAll(mw);
}

void
scroll(XtPointer data)
{
    Widget mw = (Widget)data;
    static int row = 0;
    static int column = 0;
    static Boolean up = False;
    static Boolean left = False;
    Boolean change = False;

    #if 0 /* Bug shows up in fixed rows */
    XtVaSetValues(mw, 
        XmNleftColumn, column,
        NULL);

    XtVaSetValues(mw, 
        XmNtopRow, row,
        NULL);
    #else /* Bug shows up in fixed columns */
    XtVaSetValues(mw, 
        XmNtopRow, row,
        NULL);

    XtVaSetValues(mw, 
        XmNleftColumn, column,
        NULL);
    #endif

    if (up) {
        if (row == 0) {
            up = False;
            change = True;
            row++;
        } else {
            row--;
        }
    } else {
        if (row == N_ROWS - N_VISIBLE - N_FIXED_ROWS) {
            up = True;
            change = True;
            row--;
        } else {
            row++;
        }
    }

    if (left) {
        if (column == 0) {
            left = False;
            change = True;
            column++;
        } else {
            column--;
        }
    } else {
        if (column == N_COLUMNS - N_VISIBLE - N_FIXED_COLUMNS) {
            left = True;
            change = True;
            column--;
        } else {
            column++;
        }
    }

    if(change){
        printf("%s %s\n",up ? "up" : "down", left ? "left" : "right");
    }

    XtAppAddTimeOut(
            XtWidgetToApplicationContext(mw), 200, (XtTimerCallbackProc)scroll,
            (XtPointer)mw);
}
