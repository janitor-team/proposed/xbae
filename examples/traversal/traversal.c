/*
 * Copyright(c) 1992 Bell Communications Research, Inc. (Bellcore)
 *                        All rights reserved
 *
 * Copyright � 2001 by the LessTif Developers
 *
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * $Id: traversal.c,v 1.9 2005/03/10 20:55:41 tobiasoed Exp $
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif
#include <stdlib.h>
#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif
#include <Xbae/Matrix.h>

static String fallback[] = {
	"Traversal*mw.rows:		10",
	"Traversal*mw.columns:		5",
	"Traversal*mw.columnWidths:	10, 5, 10, 15, 1",
/*
 * We need translations on the TextField edit widget to do our custom
 * traversal (EditCell(Return))
 */
	"Traversal*mw.textTranslations:	#override\\n"
	"			Shift ~Ctrl ~Meta ~Alt <Key>Tab:   EditCell(Left)\\n"
	"			~Ctrl ~Meta ~Alt <Key>Tab:	   EditCell(Right)\\n"
	"			<Key>osfUp:			   EditCell(Up)\\n"
	"			<Key>osfDown:			   EditCell(Down)\\n"
	"			<Key>osfActivate:		   EditCell(Return)\\n"
	"			~Shift ~Meta ~Alt <Key>Return:	   EditCell(Return)\\n"
	"			<Key>osfCancel:			   CancelEdit(False)",
	NULL
};

/*
 * Custom traversal.
 * Defines a new type of traversal.  Pressing the Return key will
 * now move down a row and back to the first column (like a carriage
 * return).
 */

void TraverseCB(Widget w, XtPointer client_data, XbaeMatrixTraverseCellCallbackStruct *call_data);

int
main(int argc, char *argv[])
{
    Widget toplevel, mw;
    XtAppContext app;

    toplevel = XtVaAppInitialize(&app, "Traversal",
				 NULL, 0,
				 &argc, argv,
				 fallback,
				 NULL);
#ifdef USE_EDITRES
    XtAddEventHandler( toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
#endif

    mw = XtVaCreateManagedWidget("mw",
				 xbaeMatrixWidgetClass, toplevel,
				 NULL);
    XtAddCallback(mw, XmNtraverseCellCallback, (XtCallbackProc)TraverseCB, NULL);

    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);
    /*NOTREACHED*/
    return 0;
}

/*
 * Custom traversal callback.  If the EditCell() action has the param
 * "Return", then we move down a row and back to the first editable
 * column.
 */
/* ARGSUSED */
void
TraverseCB(Widget w, XtPointer client_data, XbaeMatrixTraverseCellCallbackStruct *call_data)
{
    static XrmQuark Qreturn = NULLQUARK;

    /*
     * Get a Quark for our special action parm (for faster comparisons)
     */
    if (Qreturn == NULLQUARK)
	Qreturn = XrmStringToQuark("Return");

    /*
     * See if this is our special quark
     */
    if (call_data->qparam != Qreturn)
	return;

    /*
     * If we are on the last row we don't move
     */
    if (call_data->row == call_data->num_rows - 1)
	return;

    /*
     * Move down a row and back to the first non-fixed column
     */
    call_data->next_row = call_data->row + 1;
    call_data->next_column = call_data->fixed_columns;

}
