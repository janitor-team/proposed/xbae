/* $Header: /cvsroot/xbae/Xbae/examples/tests/0rows.c,v 1.2 2005/03/28 17:47:33 tobiasoed Exp $
 * if a table is initialized, filled with some rows, cleared to 0 rows (is empty) 
 * and then realized and mapped, a sigsev error occures,
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <Xm/Xm.h>
#include <Xbae/Matrix.h>

#include <XbaeConfig.h>

static String fallback[] = {
        "XbaeTest.height:           250",
        "XbaeTest.width:            250",
        NULL
};

String	rows[] = { "1" , "a" , "3" , "b" , NULL};

int
main(int argc, char *argv[])
{
	XtAppContext app;
	Widget shell, w;

	shell = XtAppInitialize(&app, "XbaeTest", NULL, 0, &argc, argv, fallback, NULL, 0);

	w = XtCreateWidget("mw", xbaeMatrixWidgetClass, shell, NULL, 0);
	XtVaSetValues(w,
			XmNrows, 0,
			XmNcolumns, 0,
			XmNcolumnLabels, 0,
			XmNxmColumnLabels, 0,
			XmNcolumnMaxLengths, 0,
			XmNcolumnAlignments, 0,
			XmNcolumnFontBold, 0,
			XmNcolumnButtonLabels, 0,
			XmNcolumnLabelAlignments, 0,
			XmNshowColumnArrows, 0,
			XmNcolumnWidths, 0,
			NULL);
	XbaeMatrixAddRows(w, 0, rows, NULL, NULL, 2);
	XbaeMatrixDeleteRows(w, 0, 2);
	XtManageChild(w);
	XtRealizeWidget(shell);
	XtAppMainLoop(app);
	exit(0);
}
