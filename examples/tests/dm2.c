/*
 * $Header: /cvsroot/xbae/Xbae/examples/tests/dm2.c,v 1.5 2005/05/15 18:16:06 dannybackx Exp $
 */
#include <stdio.h>
#include <stdlib.h>
#include <Xbae/Matrix.h>
#include <Xm/Text.h>
#include <XbaeConfig.h>
#ifdef	WITH_DMALLOC
#include <dmalloc.h>
#endif

XtAppContext app;
Widget toplevel, mw = 0;

static String fallback[] = {
        "Matrixwidget.height:           250",
        "Matrixwidget.width:            250",
        "Matrixwidget*mw.value:         XBAE MATRIX MEMORY FEVER!!!",
        "Matrixwidget*mw.columns:       4",
        "Matrixwidget*mw.rows:          4",
        "Matrixwidget*mw.cells:         1, 2, 3, 4\\n"
        "                               1, 2, 3, 4\\n"
        "                               1, 2, 3, 4\\n"
        "                               1, 2, 3, 4\\n"
        "                               1, 2, 3, 4",
	NULL
};

unsigned long mark = 0;
unsigned int  count = 10;

void
timeout(XtPointer cd, XtIntervalId* id)
{
	if(mw) XtDestroyWidget(mw);

#ifdef	WITH_DMALLOC
	dmalloc_log_changed(mark, 1, 1, 1);
#endif
	if (count-- == 0)
		exit(0);
#ifdef	WITH_DMALLOC
	mark = dmalloc_mark();
#endif
    	mw = XtVaCreateManagedWidget("mw",
/*				 xmTextWidgetClass, toplevel, */
				 xbaeMatrixWidgetClass, toplevel,
				 NULL);
    	XtAppAddTimeOut( app, 100, timeout, NULL);
}

int
main(int argc, char *argv[])
{
#ifndef	WITH_DMALLOC
	fprintf(stderr, "This test only makes sense when compiled with dmalloc.\n");
	exit(1);
#else
    toplevel = XtVaAppInitialize(&app, "Matrixwidget",
				 NULL, 0,
				 &argc, argv,
#if 0
				 NULL,
#else
				 fallback,
#endif
				 NULL);
    
    XtAppAddTimeOut( app, 100, timeout, NULL);
    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);
    
    /*NOTREACHED*/
    return 0;
#endif
}
