/*
 * $Header: /cvsroot/xbae/Xbae/examples/tests/images.c,v 1.3 2005/03/15 18:15:20 tobiasoed Exp $
 *
 * Show a number of cells with images in them.
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif
#include <stdlib.h>
#include <Xbae/Matrix.h>
#include <Xm/XmAll.h>
#include <X11/xpm.h>

static String fallback[] = {
	"Matrix*mw.rowLabels:		1, 2, 3, 4, 5, 6",
	"Matrix*mw.allowColumnResize:	True",
/*	"Matrix*mw.columnWidths:	40, 30, 50", */
	"Matrix*cellShadowThickness:	2",
	"Matrix*cellMarginWidth:	0",
	"Matrix*cellMarginHeight:	3",
/*	"Matrix*gridType:		grid_none", */
	"Matrix*cellShadowType:		shadow_in",
	"Matrix*rowLabelColor:		Red",
	"Matrix*columnLabelColor:	Blue",
	"Matrix*mw.buttonLabels:	True",
	"Matrix*mw.allowColumnResize:	True",
	NULL
};

int main(int argc, char *argv[])
{
	Widget toplevel, mw;
	XtAppContext app;
	int i, j;

	toplevel = XtVaAppInitialize(&app, "Matrix", NULL, 0, &argc, argv,
		fallback, NULL);
	mw = XtVaCreateManagedWidget("mw", xbaeMatrixWidgetClass, toplevel,
		XmNrows,		5,
		XmNvisibleRows,	5,
		XmNcolumns,		3,
		NULL);

	XtRealizeWidget(toplevel);

	for (i=0; i<5; i++) {
		Pixmap	p;
		char	*fn;
		int	r, hx, hy;
		unsigned int	wid, ht;

		switch(i) {
		case 0:	fn = "/usr/X11R6/include/X11/bitmaps/woman"; break;
		case 1:	fn = "/usr/X11R6/include/X11/bitmaps/plaid"; break;
		case 2:	fn = "/usr/X11R6/include/X11/bitmaps/keyboard16"; break;
		case 3:	fn = "/usr/X11R6/include/X11/bitmaps/mailempty"; break;
		default:
		case 4:	fn = "/usr/X11R6/include/X11/bitmaps/xlogo32";
		}
		p = (Pixmap)NULL;
		r = XpmReadFileToPixmap(XtDisplay(mw), XtWindow(mw),
				fn,
				&p, NULL, NULL);
		if (r == XpmFileInvalid) {
			/* maybe it wasn't an XPM file */
			r = XReadBitmapFile(XtDisplay(mw), XtWindow(mw), fn,
					&wid, &ht, &p, &hx, &hy);
			if (r != BitmapSuccess)
				p = (Pixmap)NULL;
		}
		if (p)
			XbaeMatrixSetCellPixmap(mw, i, 1, p, (Pixmap)NULL);
		for (j=0; j<3; j++)
			if (j != 1) {
				char s[16];
				sprintf(s, "cell %d-%d", i, j);
				XbaeMatrixSetCell(mw, i, j, s);
			}
	}

	XtAppMainLoop(app);

	/*NOTREACHED*/
	return 0;
}

