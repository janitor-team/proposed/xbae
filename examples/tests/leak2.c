/* $Header: /cvsroot/xbae/Xbae/examples/tests/leak2.c,v 1.3 2003/06/21 09:38:46 dannybackx Exp $
   From:        Yasushi Yamasaki <yamapu@osk3.3web.ne.jp>
   To:          lesstif@hungry.com
   Subject:     bug report
   Date:        Sun, 30 Aug 1998 23:55:47 +0900
   Cc:          yamapu@osk3.3web.ne.jp
 */

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>

#include <Xm/Xm.h>
#include <Xbae/Matrix.h>

typedef struct AppRes {
	int	numIter;
} AppRes;
AppRes	appres;

XtResource	resources[] = {
	{ "numIter", "NumIter", XtRInt, sizeof(int),
		XtOffsetOf(AppRes, numIter), XtRImmediate, 1000 },
};

int
main(int argc, char *argv[])
{
	XtAppContext app;
	Widget shell, w;
	void *before;
	void *after;
	int iter = 0;
	int diff = 0;
	int total = 0;

	shell = XtAppInitialize(&app, "Test", NULL, 0,
		&argc, argv, NULL, NULL, 0);
	XtVaGetApplicationResources(shell, &appres, resources, XtNumber(resources), NULL);

	while (iter < appres.numIter) {
		before = sbrk((ptrdiff_t) 0);
		w = XtCreateWidget("name", xbaeMatrixWidgetClass, shell, NULL, 0);
		XtDestroyWidget(w);
		after = sbrk((ptrdiff_t) 0);
		if ((int)((char *)after - (char *)before) > 0) {
			if (iter != 0) {
				/*
				printf("%i %i %p %i\n",
				iter, iter - diff, after - before,
				(after - before) / (iter - diff));
				*/
				total += (int)((char *)after - (char*)before);
			}
			diff = iter;
		}
		iter++;
	}
	printf("leaking %i bytes per Create/Destroy\n", total / iter);
	exit((int)(total / iter));
}
