!
! Sample WML file to integrate the Xbae widgets into the Builder Xcessory
!
! $Header: /cvsroot/xbae/Xbae/examples/builderXcessory/xbae.wml.in,v 1.8 2003/10/20 19:11:45 dannybackx Exp $
!
DataType
	boolean_table {
		TypeName = "BooleanTable";
		TypeSize = sizeofPointer;
	};
	shadowtype_table {
		TypeName = "ShadowTypeTable";
		TypeSize = sizeofPointer;
	};
	widget_table {
		TypeName = "WidgetTable";
		TypeSize = sizeofPointer;
	};
	userdata_table {
		TypeName = "UserDataTable";
		TypeSize = sizeofPointer;
	};

Resource
!
! XbaeMatrix resources, in several categories.
!
! Callback resources
!
    XmNdefaultActionCallback : Argument {
        Type = reason;
    };
    XmNdrawCellCallback : Argument {
        Type = reason;
    };
    XmNenterCellCallback : Argument {
        Type = reason;
    };
    XmNlabelActivateCallback : Reason {
        ActionView = True;
    };
    XmNleaveCellCallback : Argument {
        Type = reason;
    };
    XmNprocessDragCallback : Argument {
        Type = reason;
    };
    XmNresizeColumnCallback : Argument {
        Type = boolean;
    };
    XmNresizeRowCallback : Argument {
        Type = boolean;
    };
    XmNselectCellCallback : Argument {
        Type = reason;
    };
    XmNtraverseCellCallback : Argument {
        Type = reason;
    };
    XmNwriteCellCallback : Argument {
        Type = reason;
    };

!
! Visual resources (excluding geometry).
!
    XmNboldLabels : Argument {
        Type = boolean;
    };
    XmNbuttonLabelBackground : Argument {
	Type = color;
    };
    XmNcellBackground : Argument {
	Type = color;
    };
    XmNcellBackgrounds : Argument {
        Type = color_table;
	Default = "Null";
    };
    XmNcellShadowType : Argument {
        Type = integer;
	TypeName = "ShadowType";
	TypeSize = sizeofChar;
	VisualView = True;
	EnumerationSet = ShadowType;
	Default = "XmSHADOW_OUT";
    };
    XmNcellShadowTypes : Argument {
        Type = integer;
    };
    XmNcolor : Argument {
	Type = color;
    };
    XmNcolors : Argument {
	Type = color_table;
	Default = "Null";
    };
    XmNcolumnAlignments : Argument {
        Type = identifier;
    };
    XmNcolumnFontBold : Argument {
	Type = boolean_table;
	Default = "Null";
	VisualView = True;
    };
    XmNcolumnLabelAlignments : Argument {
        Type = identifier;
    };
    XmNcolumnLabelColor : Argument {
        Type = color;
    };
    XmNevenRowBackground : Argument {
        Type = color;
    };
    XmNlabelAlignment : Argument {
	Type = integer;
	EnumerationSet = Alignment;
    };
    XmNlabelFont : Argument {
        Type = font_table;
    };
    XmNlabelPosition : Argument {
	Type = integer;
	EnumerationSet = XbaeLabelPosition;
    };
    XmNlabelTextAlignment : Argument {
	Type = integer;
	EnumerationSet = Alignment;
    };
    XmNoddRowBackground : Argument {
        Type = color;
    };
    XmNrowLabelAlignment : Argument {
        Type = identifier;
    };
    XmNrowLabelColor : Argument {
        Type = color;
    };
    XmNselectedBackground : Argument {
        Type = color;
    };
    XmNselectedForeground : Argument {
        Type = color;
    };
    XmNtextBackground : Argument {
        Type = color;
    };
    XmNvisibleColumns : Argument {
        Type = integer;
    };
    XmNvisibleRows : Argument {
        Type = integer;
    };

!
! Geometry resources
!
    XmNcellMarginHeight : Argument {
        Type = integer;
    };
    XmNcellMarginWidth : Argument {
        Type = integer;
    };
    XmNcellHighlightThickness : Argument {
        Type = integer;
    };
    XmNcellShadowThickness : Argument {
        Type = integer;
    };
    XmNcolumnWidths : Argument {
	Type = identifier;
	XtRname = "WidthArray";
    };
    XmNrowHeights : Argument {
	Type = dimension_table;
	Default = "Null";
	VisualView = True;
    };
    XmNrowLabelWidth : Argument {
        Type = integer;
    };
    XmNtextShadowThickness : Argument {
        Type = integer;
	TypeName = "Dimension";
	VisualView = True;
	TypeSize = sizeofShort;
    };

!
! Options
!
    XmNallowColumnResize : Argument {
        Type = boolean;
    };
    XmNallowRowResize : Argument {
	Type = boolean;
    };
    XmNbuttonLabels : Argument {
    	Type = boolean;
    };
    XmNcalcCursorPosition : Argument {
	Type = boolean;
    };
    XmNshowArrows : Argument {
	Type = boolean;
	VisualView = True;
    };
    XmNshowColumnArrows : Argument {
	Type = boolean_table;
	Default = "Null";
	VisualView = True;
    };
    XmNrowButtonLabels : Argument {
	Type = boolean_table;
	Default = "Null";
	VisualView = True;
    };
    XmNcolumnButtonLabels : Argument {
	Type = boolean_table;
	Default = "Null";
	VisualView = True;
    };
    XmNmultiLineCell : Argument {
        Type = boolean;
    };
    XmNfill : Argument {
        Type = boolean;
    };
    XmNhorzFill : Argument {
        Type = boolean;
    };
    XmNvertFill : Argument {
        Type = boolean;
    };
    XmNuseXbaeInput : Argument {
    	Type = boolean;
    };
    XmNselectScrollVisible : Argument {
        Type = boolean;
    };

!
! Other resources
!

    XmNcolumnLabels : Argument {
        Type = identifier;
	XtRname = "StringArray";
    };
    XmNselectedCells : Argument {
        Type = identifier;
    };
    XmNhighlightedCells : Argument {
        Type = identifier;
    };
    XmNtrailingFixedRows : Argument {
        Type = integer;
    };
    XmNfixedRows : Argument {
        Type = integer;
    };
    XmNrowShadowTypes : Argument {
        Type = identifier;
    };
    XmNscrollBackground : Argument {
	Type = color;
    };
    XmNcolumns : Argument {
	Type = integer;
    };
    XmNcolumnShadowTypes : Argument {
        Type = shadowtype_table;
    };
    XmNtrailingFixedColumns : Argument {
        Type = integer;
    };
    XmNfixedColumns : Argument {
        Type = integer;
    };
    XmNleftColumn : Argument {
        Type = integer;
    };
    XmNrowLabels : Argument {
        Type = asciz_table;
    };
    XmNcells : Argument {
        Type = identifier;
    };
    XmNspace : Argument {
        Type = integer;
    };
    XmNgridType : Argument {
        Type = integer; EnumerationSet = GridType;
    };
    XmNreverseSelect : Argument {
        Type = boolean;
    };
    XmNaltRowCount : Argument {
        Type = integer;
    };
    XmNtopRow : Argument {
        Type = integer;
    };
    XmNtrailingAttachedBottom : Argument {
	Type = boolean;
    };
    XmNtrailingAttachedRight : Argument {
	Type = boolean;
    };
    XmNcolumnMaxLengths : Argument {
        Type = identifier;
	XtRname = "MaxLengthArray";
    };
    XmNcellShadowType : Argument {
        Type = integer;
	XtRname = ShadowType;
    };
    XmNverticalScrollBarDisplayPolicy : Argument {
        Type = integer; EnumerationSet = ScrollBar;
    };
    XmNhorizontalScrollBarDisplayPolicy : Argument {
        Type = integer; EnumerationSet = ScrollBar;
    };
    XmNrowUserData : Argument {
        Type = userdata_table;
    };
    XmNcolumnUserData : Argument {
        Type = userdata_table;
    };
    XmNcellUserData : Argument {
        Type = identifier;
    };
    XmNcellWidgets : Argument {
	Type = widget_table;
	Default = "Null";
    };
    XmNtextField : Argument {
        Type = widget_ref;
    };
    XmNgridLineColor : Argument {
        Type = color;
    };

    XmNoverwriteMode : Argument {
	Type = boolean;
    };
    XmNconvertCase : Argument {
	Type = boolean;
    };

    XmNcolumnWidthInPixels : Argument {
	Type = boolean;	
    };
    XmNtraverseFixedCells : Argument {
	Type = boolean;	
    };

!
! XbaeInput resources
!
    XmNpattern : Argument {
	Type = string;
    };

Class XbaeMatrixClass : Widget
{
        IncludeFile = "<Xbae/Matrix.h>";
	XtLiteral = "xbaeMatrixWidgetClass";
	LiveObject = True;
        SuperClass = "XmManager";
	LoadLibrary = "/usr/local/lib/libXbae.so";
	LinkLibrary = "-lXbae";
	ConvenienceFunction = "XbaeCreateMatrix";
        Resources {
	    XmNallowRowResize;
	    XmNbuttonLabelBackground;
	    XmNbuttonLabels;
	    XmNcalcCursorPosition;
	    XmNcellBackground;
	    XmNcellWidgets;
	    XmNcolor;
	    XmNcolumnButtonLabels;
	    XmNcolumnFontBold;
	    XmNcolumnWidthInPixels;
	    XmNcolumnWidths;
	    XmNfill;
	    XmNhorzFill;
	    XmNlabelActivateCallback;
	    XmNmultiLineCell;
	    XmNresizeRowCallback;
	    XmNrowButtonLabels;
	    XmNrowHeights;
	    XmNscrollBackground;
	    XmNscrollBarPlacement;
	    XmNselectionPolicy;
	    XmNshadowThickness;
	    XmNshowArrows;
	    XmNshowColumnArrows;
	    XmNtrailingAttachedBottom;
	    XmNtrailingAttachedRight;
	    XmNtraverseFixedCells;
	    XmNuseXbaeInput;
	    XmNvalueChangedCallback;
	    XmNvertFill;
            XmNallowColumnResize;
            XmNaltRowCount;
            XmNboldLabels;
            XmNcellBackgrounds;
            XmNcellHighlightThickness;
            XmNcellMarginHeight;
            XmNcellMarginWidth;
            XmNcellShadowThickness;
            XmNcellShadowType;
            XmNcellShadowTypes;
            XmNcellUserData;
            XmNcells;
            XmNclipWindow;
            XmNcolors;
            XmNcolumnAlignments;
            XmNcolumnLabelAlignments;
            XmNcolumnLabelColor;
            XmNcolumnLabels;
            XmNcolumnMaxLengths;
            XmNcolumnShadowTypes;
            XmNcolumnUserData;
            XmNcolumnWidths;
            XmNcolumns { Default = "5"; };
            XmNdefaultActionCallback;
            XmNdoubleClickInterval;
            XmNdrawCellCallback;
            XmNenterCellCallback;
            XmNevenRowBackground;
            XmNfixedColumns;
            XmNfixedRows;
            XmNfontList;
            XmNgridLineColor;
            XmNgridType;
            XmNhighlightedCells;
            XmNhorizontalScrollBar;
            XmNhorizontalScrollBarDisplayPolicy;
            XmNlabelFont;
            XmNleaveCellCallback;
            XmNleftColumn;
            XmNmodifyVerifyCallback;
            XmNoddRowBackground;
            XmNprocessDragCallback;
            XmNresizeCallback;
            XmNresizeColumnCallback;
            XmNreverseSelect;
            XmNrowLabelAlignment;
            XmNrowLabelColor;
            XmNrowLabelWidth;
            XmNrowLabels;
            XmNrowShadowTypes;
            XmNrowUserData;
            XmNrows { Default = "5"; };
            XmNselectCellCallback;
            XmNselectScrollVisible;
            XmNselectedBackground;
            XmNselectedCells;
            XmNselectedForeground;
            XmNshadowType;
            XmNspace;
            XmNtextBackground;
            XmNtextField;
            XmNtextTranslations;
            XmNtopRow;
            XmNtrailingFixedColumns;
            XmNtrailingFixedRows;
            XmNtraverseCellCallback;
            XmNverticalScrollBar;
            XmNverticalScrollBarDisplayPolicy;
            XmNvisibleColumns;
            XmNvisibleRows;
            XmNwriteCellCallback;
        };
    };

Class XbaeCaptionClass : Widget
{
        IncludeFile = "<Xbae/Caption.h>";
	XtLiteral = "xbaeCaptionWidgetClass";
	LiveObject = True;
        SuperClass = "XmManager";
	LoadLibrary = "/usr/local/lib/libXbae.so";
	LinkLibrary = "-lXbae";
	ConvenienceFunction = "XbaeCreateCaption";
        Resources {
	    XmNfontList;
	    XmNlabelAlignment;
	    XmNlabelOffset;
	    XmNlabelPixmap;
	    XmNlabelPosition;
	    XmNlabelString;
	    XmNlabelTextAlignment;
	    XmNlabelType;
        };
    };

Class XbaeInputClass : Widget
{
        IncludeFile = "<Xbae/Input.h>";
	XtLiteral = "xbaeInputWidgetClass";
	LiveObject = True;
        SuperClass = "XmText";
	LoadLibrary = "/usr/local/lib/libXbae.so";
	LinkLibrary = "-lXbae";
	ConvenienceFunction = "XbaeCreateInput";
        Resources {
	    XmNalignment;
	    XmNautoFill;
	    XmNconvertCase;
	    XmNoverwriteMode;
	    XmNpattern;
	    XmNvalidateCallback;
        };
    };

ControlList
        AllWidgetsAndGadgets {
            AllWidgetsAndGadgets;
            "XbaeMatrixClass";
	};

ControlList
        AllWidgets {
            AllWidgets;
            "XbaeMatrixClass";
	};

EnumerationSet
	GridType : integer {
		XmGRID_NONE;
		XmGRID_LINE;
		XmGRID_SHADOW_IN;
		XmGRID_SHADOW_OUT;
		XmGRID_ROW_SHADOW;
		XmGRID_COLUMN_SHADOW;
	};
	ScrollBar : integer {
		XmDISPLAY_NONE;
		XmDISPLAY_AS_NEEDED;
		XmDISPLAY_STATIC;
	};
	CellShadowType : integer {
		XmSHADOW_ETCHED_IN=5;
		XmSHADOW_ETCHED_OUT; 
		XmSHADOW_IN;
		XmSHADOW_OUT;
	};
	XbaeLabelPosition : integer {
		XbaePositionLeft;
		XbaePositionRight;
		XbaePositionTop;
		XbaePositionBottom;
	};
