/*
 * Copyright(c) 1992 Bell Communications Research, Inc. (Bellcore)
 *                        All rights reserved
 *
 * Copyright � 2001 by the LessTif Developers.
 *
 * Permission to use, copy, modify and distribute this material for
 * any purpose and without fee is hereby granted, provided that the
 * above copyright notice and this permission notice appear in all
 * copies, and that the name of Bellcore not be used in advertising
 * or publicity pertaining to this material without the specific,
 * prior written permission of an authorized representative of
 * Bellcore.
 *
 * BELLCORE MAKES NO REPRESENTATIONS AND EXTENDS NO WARRANTIES, EX-
 * PRESS OR IMPLIED, WITH RESPECT TO THE SOFTWARE, INCLUDING, BUT
 * NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND
 * FITNESS FOR ANY PARTICULAR PURPOSE, AND THE WARRANTY AGAINST IN-
 * FRINGEMENT OF PATENTS OR OTHER INTELLECTUAL PROPERTY RIGHTS.  THE
 * SOFTWARE IS PROVIDED "AS IS", AND IN NO EVENT SHALL BELLCORE OR
 * ANY OF ITS AFFILIATES BE LIABLE FOR ANY DAMAGES, INCLUDING ANY
 * LOST PROFITS OR OTHER INCIDENTAL OR CONSEQUENTIAL DAMAGES RELAT-
 * ING TO THE SOFTWARE.
 *
 * $Id: dynamic.c,v 1.7 2001/03/30 07:52:02 dannybackx Exp $
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif
#include <stdlib.h>
#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif
#include <Xm/RowColumn.h>
#include <Xm/Label.h>
#include <Xm/PushB.h>
#include <Xbae/Matrix.h>

static String fallback[] = {
	"Dynamic*mw.rows:			6",
	"Dynamic*mw.columns:			5",
	"Dynamic*mw.columnWidths:		5, 15, 10, 10, 15",
	"Dynamic*mw.rowLabels:			Row1, Row2, Row3, Row4, Row5, RowSix",
	"Dynamic*mw.columnLabels:		Col1, Col2, Col3, Col4, ColFive",
	"Dynamic*mw.visibleRows:		6",
	NULL
};

/*
 * Dynamically expand the matrix when tabbing past the last row or column.
 */


void TraverseCB(Widget w, XtPointer client_data, XbaeMatrixTraverseCellCallbackStruct *call_data);

int
main(int argc, char *argv[])
{
    Widget toplevel, mw;
    XtAppContext app;

    toplevel = XtVaAppInitialize(&app, "Dynamic",
				 NULL, 0,
				 &argc, argv,
				 fallback,
				 NULL);
#ifdef USE_EDITRES
    XtAddEventHandler( toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
#endif

    mw = XtVaCreateManagedWidget("mw",
				 xbaeMatrixWidgetClass,	toplevel,
				 NULL);
    XtAddCallback(mw, XmNtraverseCellCallback, (XtCallbackProc)TraverseCB, NULL);

    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);
    /*NOTREACHED*/
    return 0;
}

/* ARGSUSED */
void
TraverseCB(Widget w, XtPointer client_data, XbaeMatrixTraverseCellCallbackStruct *call_data)
{
    static XrmQuark QRight = NULLQUARK;
    static XrmQuark QDown = NULLQUARK;

    /*
     * Get the Quarks we care about
     */
    if (QRight == NULLQUARK) {
	QRight = XrmStringToQuark("Right");
	QDown = XrmStringToQuark("Down");
    }

    /*
     * If we are moving down, and we are at the last row, add a new row
     * and traverse to it.
     */
    if (call_data->qparam == QDown &&
	call_data->row == call_data->num_rows - 1) {
	XbaeMatrixAddRows(w, call_data->num_rows, NULL, NULL, NULL, 1);
	call_data->next_row = call_data->num_rows;
	call_data->next_column = call_data->column;
    }
    /*
     * If we are moving right, and we are at the last column, add a new column
     * and traverse to it.
     */
    else if (call_data->qparam == QRight &&
	call_data->column == call_data->num_columns - 1) {
	short width = 10;
	XbaeMatrixAddColumns(w, call_data->num_columns, NULL, NULL,
			     &width, NULL, NULL, NULL, NULL, 1);
	call_data->next_column = call_data->num_columns;
	call_data->next_row = call_data->row;
    }
}
