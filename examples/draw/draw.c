/*
 * AUTHOR: Andrew Lister <lister@syd.net.au>
 *
 * Simple example of the draw cell, write cell routines
 *
 * $Id: draw.c,v 1.16 2005/03/08 01:00:39 tobiasoed Exp $
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif

#include <Xm/Text.h>
#include <Xm/Form.h>
#include <Xbae/Matrix.h>

#if XmVersion >= 2001
# define HAVE_XPM
# include <Xm/XpmP.h>
#elif defined(HAVE_XPM_H)
# define HAVE_XPM
# include <xpm.h>
#elif defined(HAVE_X11_XPM_H)
# define HAVE_XPM
# include <X11/xpm.h>
#endif

#define ROWS 10000
#define COLUMNS 20
#define WIDTH 8

static char cells[ ROWS ][ COLUMNS ][ WIDTH ];
static Pixmap pixmap = 0, mask;
static Widget text;
static int lastRow = -1;

static void DrawCB(Widget w, XtPointer cd, XbaeMatrixDrawCellCallbackStruct *cbs);
static void WriteCB(Widget w, XtPointer cd, XbaeMatrixWriteCellCallbackStruct *cbs);
static void EnterCB(Widget w, XtPointer cd, XbaeMatrixEnterCellCallbackStruct *cbs);
static void LeaveCB(Widget w, XtPointer cd, XbaeMatrixLeaveCellCallbackStruct *cbs);
static void LoadMatrix(void);

#ifdef HAVE_XPM
/* XPM */
static char * xbae_xpm[] =
{
    "48 16 5 1",
    " 	s mask	c None",
    ".	c Red",
    "X	c Blue",
    "o	c Green",
    "O	c Yellow",
    "....           .                                ",
    " ....          .                                ",
    "  ....        .                                 ",
    "   ....      .   XXXXX   oooo  OOOOO            ",
    "   ....     .    XX  XX oo  oo OO               ",
    "    ....   .     XX  XX oo  oo OO               ",
    "     ....  .     XX  XX oo  oo OO               ",
    "      ... .      XXXXX  oooooo OOOO             ",
    "      .. .       XX  XX oo  oo OO               ",
    "      . ...      XX  XX oo  oo OO               ",
    "     .  ....     XX  XX oo  oo OO               ",
    "     .   ....    XX  XX oo  oo OO               ",
    "    .    ....    XXXXX  oo  oo OOOOO            ",
    "   .      ....                                  ",
    "  .        ....                                 ",
    " .          ....                                "
};
#endif

#define xbae_width 48
#define xbae_height 16
static unsigned char xbae_bits[] = {
   0x0f, 0x80, 0x00, 0x00, 0x00, 0x00, 0x1e, 0x80, 0x00, 0x00, 0x00, 0x00,
   0x3c, 0x40, 0x00, 0x00, 0x00, 0x00, 0x78, 0x20, 0x3e, 0x9e, 0x0f, 0x00,
   0x78, 0x10, 0x66, 0xb3, 0x01, 0x00, 0xf0, 0x08, 0x66, 0xb3, 0x01, 0x00,
   0xe0, 0x09, 0x66, 0xb3, 0x01, 0x00, 0xc0, 0x05, 0x3e, 0xbf, 0x07, 0x00,
   0xc0, 0x02, 0x66, 0xb3, 0x01, 0x00, 0x40, 0x07, 0x66, 0xb3, 0x01, 0x00,
   0x20, 0x0f, 0x66, 0xb3, 0x01, 0x00, 0x20, 0x1e, 0x66, 0xb3, 0x01, 0x00,
   0x10, 0x1e, 0x3e, 0xb3, 0x0f, 0x00, 0x08, 0x3c, 0x00, 0x00, 0x00, 0x00,
   0x04, 0x78, 0x00, 0x00, 0x00, 0x00, 0x02, 0xf0, 0x00, 0x00, 0x00, 0x00
};

static String fallback[] = {
	"Draw*mw.cellHighlightThickness:	2",
	"Draw*mw.cellMarginWidth:		3",
	"Draw*mw.cellMarginHeight:		5",
	"Draw*mw.fixedRows:			2",
	"Draw*mw.fixedColumns:			1",
	"Draw*mw.traverseFixedCells:		True",
	"Draw*mw.gridType:			grid_cell_shadow",
	"Draw*mw.fontList:			-*-helvetica-medium-r-*-*-14-*-*-*-*-*-*-*",
	"Draw*mw.allowColumnResize:		True",
	"Draw*mw.selectScrollVisible:		False",
	NULL
};

static void
LoadMatrix(void)
{
    int i, j;

    for( i = 0; i < ROWS; i++ )
	for( j = 0; j < COLUMNS; j++ )
	    ( void )sprintf( cells[ i ][ j ], "%d %d", i, j );
}

static void
DrawCB(Widget w, XtPointer cd, XbaeMatrixDrawCellCallbackStruct *cbs)
{

    fprintf(stderr,"drawCB %d %d\n",cbs->row,cbs->column);

    if( cbs->column == 2 ) {
	    if( !pixmap ) {
#ifdef HAVE_XPM
		XpmAttributes attributes;

		attributes.valuemask = 0;
		attributes.valuemask |= XpmReturnInfos;
#if XpmIncludeVersion > 30406
		attributes.valuemask |= XpmReturnAllocPixels;
#endif
		attributes.valuemask |= XpmReturnExtensions;
		
		if( XpmCreatePixmapFromData(
		    XtDisplay(w), XtWindow(w), xbae_xpm, &pixmap, &mask,
		    &attributes) != XpmSuccess )
		    
		    /* Use a bitmap instead */
		    pixmap = XCreateBitmapFromData(
			XtDisplay( w ), RootWindowOfScreen( XtScreen( w ) ),
			( char * )xbae_bits, xbae_width, xbae_height );
		
		XpmFreeAttributes( &attributes );
#else    
		pixmap = XCreateBitmapFromData(
		    XtDisplay( w ), RootWindowOfScreen( XtScreen( w ) ),
		    ( char * )xbae_bits, xbae_width, xbae_height );
#endif
	    }

		/* Assign it a pixmap */
		cbs->type = XbaePixmap;
		cbs->pixmap = pixmap;
		cbs->mask = mask;

		/* Set height and width to that of the bitmap's to ensure correct
		   positioning and drawing */

		/* This is no longer necessary as it is calculated on the fly.
		   However, providing the values improves efficiency */
		cbs->height = xbae_height;
		cbs->width = xbae_width;
#ifdef HAVE_XPM
		cbs->depth = 8;
#else
		cbs->depth = 1;
#endif
    } else {
	cbs->type = XbaeString;
	cbs->string = cells[ cbs->row ][ cbs->column ];
    }
    return;
}

static void
WriteCB(Widget w, XtPointer cd, XbaeMatrixWriteCellCallbackStruct *cbs)
{
    char buf[ 128 ];
    printf("Write\n");
    
    if ( cbs->column == 0 ) {
	strcpy( buf, "Cells containing Pixmaps cannot change" );
    } else {
	if( strcmp( cells[ cbs->row ][ cbs->column ], cbs->string ) == 0 ) {
	    sprintf( buf, "No change made to cell [%d, %d]", cbs->row,
		     cbs->column );
	} else {
	    sprintf( buf, "Changing value in [%d, %d] from %s to %s",
		     cbs->row, cbs->column, cells[ cbs->row ][ cbs->column ],
		     cbs->string );

	    strcpy( cells[ cbs->row ][ cbs->column ], cbs->string );
	}
    }
    XmTextSetString( text, buf );
}

static void
EnterCB(Widget w, XtPointer cd, XbaeMatrixEnterCellCallbackStruct *cbs)
{
    printf("Enter\n");
    if( lastRow == cbs->row )
	return;

    if( lastRow >= 0 ) {
	XbaeMatrixUnhighlightRow( w, lastRow );
    }
    
    XbaeMatrixHighlightRow( w, cbs->row );

    lastRow = cbs->row;
}

static void
LeaveCB(Widget w, XtPointer cd, XbaeMatrixLeaveCellCallbackStruct *cbs)
{
	printf("Leave\n");
}

static void
VerifyCB(Widget w, XtPointer cd,XbaeMatrixModifyVerifyCallbackStruct *cbs)
{
	printf("Verify\n");
}

int
main(int argc, char *argv[])
{
    Widget toplevel, mw, form;
    XtAppContext app;
    int i;
    short widths[ COLUMNS ];
    char buf[ 128 ];

    LoadMatrix();
    
    toplevel = XtVaAppInitialize(&app, "Draw",
				 NULL, 0,
				 &argc, argv,
				 fallback,
				 NULL);
#ifdef USE_EDITRES
    XtAddEventHandler( toplevel, (EventMask)0, True,
                       _XEditResCheckMessages, NULL);
#endif

    for( i = 0; i < COLUMNS; i++ )
	widths[ i ] = WIDTH;
    
    form = XtVaCreateManagedWidget(
	"form", xmFormWidgetClass, toplevel,
	NULL );
    
    mw = XtVaCreateManagedWidget(
	"mw", xbaeMatrixWidgetClass, form,
	XmNrows, ROWS,
	XmNcolumns, COLUMNS,
	XmNcolumnWidths, widths,
	XmNvisibleRows, 8,
	XmNvisibleColumns, 10,
	XmNleftAttachment, XmATTACH_FORM,
	XmNrightAttachment, XmATTACH_FORM,
	XmNtopAttachment, XmATTACH_FORM,
	NULL);
    XtAddCallback( mw, XmNdrawCellCallback,  (XtCallbackProc)DrawCB, NULL );
    XtAddCallback( mw, XmNwriteCellCallback, (XtCallbackProc)WriteCB, NULL );
    XtAddCallback( mw, XmNenterCellCallback, (XtCallbackProc)EnterCB, NULL );
    XtAddCallback( mw, XmNleaveCellCallback, (XtCallbackProc)LeaveCB, NULL );
    XtAddCallback( mw, XmNmodifyVerifyCallback, (XtCallbackProc)VerifyCB, NULL );

    sprintf( buf,
	     "A matrix widget with %d rows and %d columns - That's %d cells!",
	     ROWS, COLUMNS, ROWS * COLUMNS );
    
    text = XtVaCreateManagedWidget(
	"text", xmTextWidgetClass, form,
	XmNeditable, False,
	XmNcursorPositionVisible, False,
	XmNtopAttachment, XmATTACH_WIDGET,
	XmNtopWidget, mw,
	XmNleftAttachment, XmATTACH_FORM,
	XmNrightAttachment, XmATTACH_FORM,
	XmNbottomAttachment, XmATTACH_FORM,
	XmNvalue, buf,
	NULL );
	
    XtRealizeWidget(toplevel);
    XtAppMainLoop(app);
    
    /*NOTREACHED*/
    return 0;
}
