/*
 * Do the same thing as draw.c but without a draw_cell_callback.
 *
 * $Id: draw3.c,v 1.3 2005/12/22 16:59:48 tobiasoed Exp $
 */

#ifdef HAVE_CONFIG_H
#include <XbaeConfig.h>
#endif
#include <stdlib.h>
#include <stdio.h>
#ifdef USE_EDITRES
#include <X11/Intrinsic.h>
#include <X11/Xmu/Editres.h>
#endif

#include <Xm/Text.h>
#include <Xm/Form.h>
#include <Xbae/Matrix.h>

#if XmVersion >= 2001
# define HAVE_XPM
# include <Xm/XpmP.h>
#elif defined(HAVE_XPM_H)
# define HAVE_XPM
# include <xpm.h>
#elif defined(HAVE_X11_XPM_H)
# define HAVE_XPM
# include <X11/xpm.h>
#endif

#define ROWS 10000
#define COLUMNS 20
#define WIDTH 8

static Pixmap pixmap = 0, mask;
static Widget text = NULL;
static int lastRow = -1;

static void EnterCB(Widget w, XtPointer cd, XtPointer cbs);
static void Track(Widget w, XtPointer cd, XtPointer cbs);
static void LeaveCB(Widget w, XtPointer cd, XtPointer cbs);

#ifdef HAVE_XPM
/* XPM */
static char * xbae_xpm[] =
{
    "48 16 5 1",
    " 	s mask	c None",
    ".	c Red",
    "X	c Blue",
    "o	c Green",
    "O	c Yellow",
    "....           .                                ",
    " ....          .                                ",
    "  ....        .                                 ",
    "   ....      .   XXXXX   oooo  OOOOO            ",
    "   ....     .    XX  XX oo  oo OO               ",
    "    ....   .     XX  XX oo  oo OO               ",
    "     ....  .     XX  XX oo  oo OO               ",
    "      ... .      XXXXX  oooooo OOOO             ",
    "      .. .       XX  XX oo  oo OO               ",
    "      . ...      XX  XX oo  oo OO               ",
    "     .  ....     XX  XX oo  oo OO               ",
    "     .   ....    XX  XX oo  oo OO               ",
    "    .    ....    XXXXX  oo  oo OOOOO            ",
    "   .      ....                                  ",
    "  .        ....                                 ",
    " .          ....                                "
};
#endif

#define xbae_width 48
#define xbae_height 16
static unsigned char xbae_bits[] = {
   0x0f, 0x80, 0x00, 0x00, 0x00, 0x00, 0x1e, 0x80, 0x00, 0x00, 0x00, 0x00,
   0x3c, 0x40, 0x00, 0x00, 0x00, 0x00, 0x78, 0x20, 0x3e, 0x9e, 0x0f, 0x00,
   0x78, 0x10, 0x66, 0xb3, 0x01, 0x00, 0xf0, 0x08, 0x66, 0xb3, 0x01, 0x00,
   0xe0, 0x09, 0x66, 0xb3, 0x01, 0x00, 0xc0, 0x05, 0x3e, 0xbf, 0x07, 0x00,
   0xc0, 0x02, 0x66, 0xb3, 0x01, 0x00, 0x40, 0x07, 0x66, 0xb3, 0x01, 0x00,
   0x20, 0x0f, 0x66, 0xb3, 0x01, 0x00, 0x20, 0x1e, 0x66, 0xb3, 0x01, 0x00,
   0x10, 0x1e, 0x3e, 0xb3, 0x0f, 0x00, 0x08, 0x3c, 0x00, 0x00, 0x00, 0x00,
   0x04, 0x78, 0x00, 0x00, 0x00, 0x00, 0x02, 0xf0, 0x00, 0x00, 0x00, 0x00
};

static String fallback[] = {
	"Draw*mw.cellHighlightThickness:	2",
	"Draw*mw.cellMarginWidth:		3",
	"Draw*mw.cellMarginHeight:		5",
	"Draw*mw.fixedRows:			2",
	"Draw*mw.fixedColumns:			1",
	"Draw*mw.traverseFixedCells:		True",
	"Draw*mw.gridType:			grid_cell_shadow",
	"Draw*mw.fontList:			-*-helvetica-medium-r-*-*-14-*-*-*-*-*-*-*",
	"Draw*mw.allowColumnResize:		True",
	"Draw*mw.selectScrollVisible:		False",
	NULL
};

static void LoadMatrix(Widget w)
{
	int	i, j;
	char	cell[16];
#ifdef HAVE_XPM
	XpmAttributes attributes;
#endif

	for (i = 0; i < ROWS; i++)
		for (j = 0; j < COLUMNS; j++)
			if (j != 2) {
				(void)sprintf(cell, "%d %d", i, j);
				XbaeMatrixSetCell(w, i, j, cell);
			} else
			/*
			 * Play an even/odd game to have half the cells in column 2 show
			 * both text and pixmap, the other half only a pixmap.
			 */
			if (i % 2 == 0) {
				(void)sprintf(cell, "        %d %d", i, j);
				XbaeMatrixSetCell(w, i, j, cell);
			}

	/*
	 * Create a pixmap to put in some cells
	 */
#ifdef HAVE_XPM
		attributes.valuemask = 0;
		attributes.valuemask |= XpmReturnInfos;
		attributes.valuemask |= XpmReturnAllocPixels;
		attributes.valuemask |= XpmReturnExtensions;

		if (XpmCreatePixmapFromData(XtDisplay(w), XtWindow(w),
			xbae_xpm, &pixmap, &mask, &attributes) != XpmSuccess) {

			/* Use a bitmap instead */
			pixmap = XCreateBitmapFromData(XtDisplay(w),
				RootWindowOfScreen(XtScreen(w)),
				(char *)xbae_bits, xbae_width, xbae_height);
		}

		XpmFreeAttributes( &attributes );
#else    
		pixmap = XCreateBitmapFromData(XtDisplay(w),
			RootWindowOfScreen(XtScreen(w)),
			(char *)xbae_bits, xbae_width, xbae_height);
#endif

	/*
	 * Put the pixmap/mask in the cells of column 2.
	 */
	for (i=0; i<ROWS; i++) {
		XbaeMatrixSetCellPixmap(w, i, 2, pixmap, mask);
	}
}

static void
Track(Widget w, XtPointer cd, XtPointer cbs)
{
	XbaeMatrixEnterCellCallbackStruct *cbp = (XbaeMatrixEnterCellCallbackStruct *)cbs;

	fprintf(stderr, "Track(%d, %d)\n", cbp->row, cbp->column);
}

static void
EnterCB(Widget w, XtPointer cd, XtPointer cbs)
{
	XbaeMatrixEnterCellCallbackStruct *cbp = (XbaeMatrixEnterCellCallbackStruct *)cbs;

	printf("Enter\n");
	if (lastRow == cbp->row )
		return;

	if (lastRow >= 0 )
		XbaeMatrixUnhighlightRow( w, lastRow );

	XbaeMatrixHighlightRow( w, cbp->row );
	lastRow = cbp->row;
}

static void
LeaveCB(Widget w, XtPointer cd, XtPointer cbs)
{
	printf("Leave\n");
}

int
main(int argc, char *argv[])
{
    Widget toplevel, mw, form;
    XtAppContext app;
    int i;
    short widths[ COLUMNS ];
    char buf[ 128 ];

    toplevel = XtVaAppInitialize(&app, "Draw", NULL, 0, &argc, argv, fallback, NULL);
#ifdef USE_EDITRES
    XtAddEventHandler(toplevel, (EventMask)0, True, _XEditResCheckMessages, NULL);
#endif

    for( i = 0; i < COLUMNS; i++ )
	widths[ i ] = WIDTH;
    
    form = XtVaCreateManagedWidget(
	"form", xmFormWidgetClass, toplevel,
	NULL );
    
    mw = XtVaCreateWidget(
	"mw", xbaeMatrixWidgetClass, form,
	XmNrows, ROWS,
	XmNcolumns, COLUMNS,
	XmNcolumnWidths, widths,
	XmNvisibleRows, 8,
	XmNvisibleColumns, 10,
	XmNleftAttachment, XmATTACH_FORM,
	XmNrightAttachment, XmATTACH_FORM,
	XmNtopAttachment, XmATTACH_FORM,
	NULL);
    XtAddCallback( mw, XmNenterCellCallback, (XtCallbackProc)EnterCB, NULL );
    XtAddCallback( mw, XmNleaveCellCallback, (XtCallbackProc)LeaveCB, NULL );
    XtAddCallback( mw, XmNtrackCellCallback, (XtCallbackProc)Track, NULL );

    XtRealizeWidget(toplevel);

    LoadMatrix(mw);
    
    XbaeMatrixDeleteRows(mw, 2, 1);
    
    sprintf( buf,
	     "A matrix widget with %d rows and %d columns - That's %d cells!",
	     ROWS, COLUMNS, ROWS * COLUMNS );
    
    text = XtVaCreateManagedWidget(
	"text", xmTextWidgetClass, form,
	XmNeditable, False,
	XmNcursorPositionVisible, False,
	XmNtopAttachment, XmATTACH_WIDGET,
	XmNtopWidget, mw,
	XmNleftAttachment, XmATTACH_FORM,
	XmNrightAttachment, XmATTACH_FORM,
	XmNbottomAttachment, XmATTACH_FORM,
	XmNvalue, buf,
	NULL );
	
    XtManageChild(mw);
    XtAppMainLoop(app);
    
    /*NOTREACHED*/
    return 0;
}
