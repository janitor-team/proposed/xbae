dnl
dnl $Id: acinclude.m4,v 1.15 2004/02/02 20:20:03 dannybackx Exp $
dnl
dnl Add macros
dnl	AC_PATH_MOTIF_DIRECT
dnl	AC_PATH_MOTIF
dnl     LT_WITH_DMALLOC
dnl     AC_HAVE_EXCTAGS
dnl     LT_HAVE_MAN2HTML
dnl	LT_HAVE_LIBXP
dnl


dnl
dnl Search for Motif by using explicit paths
dnl
AC_DEFUN([AC_PATH_MOTIF_DIRECT],
[test -z "$motif_direct_test_library" && motif_direct_test_library=Xm
test -z "$motif_direct_test_function" && motif_direct_test_function=XmCreatePushButton
test -z "$motif_direct_test_include" && motif_direct_test_include=Xm/Xm.h
  for ac_dir in               \
    /usr/include/Motif1.2     \
    /usr/Motif1.2/include     \
                              \
    /usr/motif/include        \
    /usr/lesstif/include        \
                              \
    /usr/X11R6/include        \
    /usr/X11R5/include        \
                              \
    /usr/include/X11R6        \
    /usr/include/X11R5        \
                              \
    /usr/local/X11R6/include  \
    /usr/local/X11R5/include  \
                              \
    /usr/local/include/X11R6  \
    /usr/local/include/X11R5  \
                              \
    /usr/X11/include          \
    /usr/include/X11          \
    /usr/local/X11/include    \
    /usr/local/include/X11    \
                              \
    /usr/X386/include         \
    /usr/x386/include         \
    /usr/XFree86/include/X11  \
                              \
    /usr/dt/include           \
    /usr/openwin/include      \
    /opt/openwin/include      \
                              \
    /usr/include              \
    /usr/local/include        \
    /usr/unsupported/include  \
    /usr/athena/include       \
    /usr/local/x11r5/include  \
    /usr/lpp/Xamples/include  \
    ; \
  do
    if test -r "$ac_dir/$motif_direct_test_include"; then
      no_motif= ac_motif_includes=$ac_dir
      break
    fi
  done

# Check for the libraries.
# See if we find them without any special options.
# Don't add to $LIBS permanently.
ac_save_LIBS="$LIBS"
LIBS="-l$motif_direct_test_library $LIBS"
# First see if replacing the include by lib works.
for ac_dir in `echo "$ac_motif_includes" | sed s/include/lib/` \
    /usr/lib/Motif1.2     \
    /usr/Motif1.2/lib     \
                          \
    /usr/motif/lib        \
    /usr/lesstif/lib        \
                          \
    /usr/X11R6/lib        \
    /usr/X11R5/lib        \
                          \
    /usr/lib/X11R6        \
    /usr/lib/X11R5        \
                          \
    /usr/local/X11R6/lib  \
    /usr/local/X11R5/lib  \
                          \
    /usr/local/lib/X11R6  \
    /usr/local/lib/X11R5  \
                          \
    /usr/X11/lib          \
    /usr/lib/X11          \
    /usr/local/X11/lib    \
                          \
    /usr/X386/lib         \
    /usr/x386/lib         \
    /usr/XFree86/lib/X11  \
                          \
    /usr/dt/lib           \
    /usr/openwin/lib      \
    /opt/openwin/lib      \
                          \
    /usr/lib              \
    /usr/local/lib        \
    /usr/unsupported/lib  \
    /usr/athena/lib       \
    /usr/local/x11r5/lib  \
    /usr/lpp/Xamples/lib  \
    ; \
do
  for ac_extension in a so sl; do
    if test -r $ac_dir/lib${motif_direct_test_library}.$ac_extension; then
      no_motif= ac_motif_libraries=$ac_dir
      break 2
    fi
  done
done
LIBS=$ac_save_LIBS])



dnl
dnl Search for Motif
dnl
AC_DEFUN([AC_PATH_MOTIF],
[AC_REQUIRE_CPP()dnl

AC_ARG_WITH(motif-includes, [  --with-motif-includes=DIR     Motif include files are in DIR])
if test -z "$with_motif_includes"; then
  motif_includes=NONE
else
  motif_includes=$with_motif_includes
fi
AC_ARG_WITH(motif-libraries, [  --with-motif-libraries=DIR    Motif library files are in DIR])
if test -z "$with_motif_libraries"; then
  motif_libraries=NONE
else
  motif_libraries=$with_motif_libraries
fi

AC_MSG_CHECKING(for Motif)
AC_ARG_WITH(motif, [  --with-motif            enable Motif tests])
if test "x$with_motif" = xno; then
  no_motif=yes
else
  if test "x$motif_includes" != xNONE && test "x$motif_libraries" != xNONE; then
    no_motif=
  else
AC_CACHE_VAL(ac_cv_path_motif,
[# One or both of these vars are not set, and there is no cached value.
no_motif=yes

    #
    # Let's try a test link. If it works this will avoid putting the
    # default paths onto the compile and link lines.
    #
    ac_save_libs="$LIBS"
    ac_save_cflags="$CFLAGS"
    ac_save_cppflags="$CPPFLAGS"
    LIBS="-lXm $X_LIBS -lXt $X_PRE_LIBS -lX11 $X_EXTRA_LIBS $LIBS"
    CFLAGS="$X_CFLAGS $CFLAGS"
    CPPFLAGS="$X_CFLAGS $CPPFLAGS"

    AC_TRY_LINK([#include <Xm/Label.h>],[Widget w; XmCreateLabel(w, "", NULL, 0);],
	[
	#
	# link passed, do nothing
	#
	no_motif="no"
	motif_includes=""
	motif_libraries=""
	],
	#
	# link failed, go search for it
	#
	AC_PATH_MOTIF_DIRECT
	) dnl AC_TRY_LINK

    LIBS="$ac_save_libs"
    CFLAGS="$ac_save_cflags"
    CPPFLAGS="$ac_save_cppflags"


if test "$no_motif" = yes; then
  ac_cv_path_motif="no_motif=yes"
else
  ac_cv_path_motif="no_motif= ac_motif_includes=$ac_motif_includes ac_motif_libraries=$ac_motif_libraries"
fi])dnl
  fi
  eval "$ac_cv_path_motif"
fi # with_motif != no

if test "$no_motif" = yes; then
  AC_MSG_RESULT(no)
else
  test "x$motif_includes" = xNONE && motif_includes=$ac_motif_includes
  test "x$motif_libraries" = xNONE && motif_libraries=$ac_motif_libraries
  ac_cv_path_motif="no_motif= ac_motif_includes=$motif_includes ac_motif_libraries=$motif_libraries"
  AC_MSG_RESULT([libraries $motif_libraries, headers $motif_includes])
fi
test "x$motif_libraries" != "xNONE" && test "x$motif_libraries" != "x" && MOTIF_LIBS=-L$motif_libraries
test "x$motif_includes" != "xNONE" && test "x$motif_includes" != "x" && MOTIF_CFLAGS=-I$motif_includes
AC_SUBST(MOTIF_LIBS)
AC_SUBST(MOTIF_CFLAGS)
])



dnl
dnl Enable malloc checker for debugging purposes
dnl See http://dmalloc.com, INSTALL(.html) for references to this.
dnl Source code which depends on this is mostly in
dnl DebugUtil.c/.h
dnl
AC_DEFUN([LT_WITH_DMALLOC],
[AC_MSG_CHECKING(if malloc debugging is wanted)
AC_ARG_WITH(dmalloc,
[  --with-dmalloc[=path]   use dmalloc, see INSTALL(.html) for reference],
[if test "$withval" = no; then
  AC_MSG_RESULT(no)
else
  if test "$withval" != yes; then
dnl  a path was given
     CFLAGS="$CFLAGS -I$withval/include -DDMALLOC_FUNC_CHECK"
dnl link libs (LDFLAGS) and programs (LIBS) with it!?
     LDFLAGS="$LDFLAGS -L$withval/lib -ldmalloc"
     LIBS="$LIBS -L$withval/lib -ldmalloc"
  else
     CFLAGS="$CFLAGS -DDMALLOC_FUNC_CHECK"
     LDFLAGS="$LDFLAGS -ldmalloc"
     LIBS="$LIBS -ldmalloc"
  fi
  AC_TRY_LINK(
  [#include <dmalloc.h>],
  [char *ptr;
  ptr=malloc(1);
  dmalloc_shutdown();]
  ,
  [AC_DEFINE(WITH_DMALLOC,1,
            [Define if using the dmalloc debugging malloc package])
  AC_MSG_RESULT(Using dmalloc)],
  AC_MSG_ERROR(dmalloc not found)
  
  )
fi],
[AC_MSG_RESULT(no)])
])



dnl
dnl Checks whether Exuberant ctags is present
dnl (see http://darren.hiebert.com/ctags/index.html)
dnl
AC_DEFUN([AC_HAVE_EXCTAGS],
[
ac_have_exctags=no
AC_CHECK_PROG(ac_have_ctags, ctags, yes, no)
if test "$ac_have_ctags" = "yes"; then
  AC_MSG_CHECKING(if ctags is actually Exuberant ctags)
  if test -z "`ctags --version 2>/dev/null | grep Exuberant`" ; then
    ac_have_exctags=no
  else
    ac_have_exctags=yes
  fi
  AC_MSG_RESULT($ac_have_exctags)
fi 
AM_CONDITIONAL(Have_Exctags, test "$ac_have_exctags" = "yes")
])


dnl
dnl Checks whether man2html is present
dnl
dnl We've taken a quick look at the world and have found that man pages
dnl in traditional man page source are a good source. This source format
dnl can be translated into e.g. HTML easily.
dnl We use our own version of Debian's man2html.
dnl

AC_DEFUN([LT_HAVE_MAN2HTML],
[
AC_MSG_CHECKING(for suitable man2html)
AC_PATH_PROG(man2html_cmd, man2html)
if test -x "$man2html_cmd"
then
  if test -z "`$man2html_cmd </dev/null 2>&1 | grep \"LessTif's man2html\"`"
  then
     dnl found wrong one
     MAN2HTML="\$(top_builddir)/scripts/man2html"
     AC_MSG_RESULT(Use LessTif one)
  else
     dnl found right one
     MAN2HTML="$man2html_cmd"
     AC_MSG_RESULT($MAN2HTML)
  fi
else
  dnl found none
  MAN2HTML="\$(top_builddir)/scripts/man2html"
dnl  AC_MSG_RESULT($MAN2HTML)
fi
AC_SUBST(MAN2HTML)
AM_CONDITIONAL(LT_BUILD_MAN2HTML, test "$MAN2HTML" = "\$(top_builddir)/scripts/man2html")
])

dnl
dnl Check for libXp
dnl In fact this check ensures that
dnl  - <X11/extensions/Print.h> and
dnl  - both libXp libXext
dnl are in place
dnl Note that a simpler check only for the libraries would not
dnl be sufficient perhaps.
dnl If the test succeeds it defines Have_Libxp within our 
dnl Makefiles. Perhaps one should immediately add those libs
dnl to link commands which include libXm version2.1?!
dnl
AC_DEFUN([LT_HAVE_LIBXP],
[AC_REQUIRE([AC_PATH_X])
AC_CACHE_CHECK(whether libXp is available, lt_cv_libxp,
[lt_save_CFLAGS="$CFLAGS"
lt_save_CPPFLAGS="$CPPFLAGS"
lt_save_LIBS="$LIBS"
LIBS="$X_LIBS -lXp -lXext -lXt $X_PRE_LIBS -lX11 $X_EXTRA_LIBS $LIBS"
CFLAGS="$X_CFLAGS $CFLAGS"
CPPFLAGS="$X_CFLAGS $CPPFLAGS"
AC_TRY_LINK([
#include <X11/Intrinsic.h>
#include <X11/extensions/Print.h>
],[
int main() {
Display *display=NULL;
short   major_version, minor_version;
Status rc;
rc=XpQueryVersion(display, &major_version, &minor_version);
exit(0);
}
],
lt_cv_libxp=yes,
lt_cv_libxp=no)
])
if test "$lt_cv_libxp" = "yes"; then
  AC_DEFINE(HAVE_LIB_XP)
  LT_HAVELIBXP=1
else
  LT_HAVELIBXP=0 
fi
AM_CONDITIONAL(Have_Libxp, test "$lt_cv_libxp" = "yes")
AC_SUBST(LT_HAVELIBXP)
CFLAGS="$lt_save_CFLAGS"
CPPFLAGS="$lt_save_CPPFLAGS"
LIBS="$lt_save_LIBS"
])

