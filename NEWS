# 
# $Header: /cvsroot/xbae/Xbae/NEWS,v 1.72 2006/05/29 13:25:06 tobiasoed Exp $
#

Xbae Release 4.60.4
-------------------
* Restore the pre 4.60.0 behaviour where column/row sizes of 0 are allowed, 
  but avoid editing hidden cells
* Honour motif-2 enableThinThickness resource (close bug #1122270) 
* Allow dynamic row/col resize by clicking in the labels 
* Changes in the default translations may break apps that install 
  their own.
* Other smaller fixes, see ChangeLog for details

Xbae Release 4.60.2
-------------------
* Introduced a new boolean resource XmNtextBackgroundIsCell that specifies if 
  the background of the text child defaults to the core background of the 
  matrix or the background of the cell being edited when XmNtextBackground
  has the value XmUNSPECIFIED_PIXEL.
* A handfull of smaller bugfixes, see ChangeLog for details

Xbae Release 4.60
-----------------
A small number of big changes, and a large number of smaller changes :
* Lots of work involving fonts, XmStrings, RenderTables, ..
* The potential for memory leaks in user code due to arcad's
  per cell change: all 2d tables returned by GetValues are now
  copies that the user should free.
* The changes in traversal: The clip widget doesn't accept focus
  anymore, it's all done with the text field. The traversalCallback
  now also gets fired for focus and loosingfocus events with new
  values for the reason field.

The details of the other changes are in the ChangeLog file.

Intermediate release 4.51.03
----------------------------
* Fix a problem with cell widgets: they weren't moved out of the way by a
  call to XbaeMatrixSetCellWidget(_,_,_,NULL).
* Fix an accident in the previous release which caused the TAB to
  stop working. Now use of the TAB key moves between cells again.
* Fix a copy paste bug in the column deletion code
* Fix setcell when new value is NULL
* Simplify cell color computation
* Fix 0 rows/cols bugs in addRows/ColsToTable introduced when that code 
  was simplified and extended
* Update the contents of the textChild when the app calls XbaeMatrixRefresh
  (useful for apps using drawCellCB)
* Add XbaeStringFree as an option to the type drawCellCallBack can use.
  It's the same as XbaeString except that the matrix widget frees
  the string when it doesn't need it anymore.
* Reworked traversal yet again to isolate clipChild from traversal 
  entirely and get rid of the racy timeout. This breaks example/list+list2, 
  but a new list3 works (even with fixed cells, which the other 2 didn't).
  It also gets unmap to work and allows to traverse over non editable
  cells/widgets in 'stelth' mode. It gets the 'edit the top row/left column'
  to work without reintroducing the 'spontaneous matrix rewind bug' that 
  had plagued xbae a while ago. The fix for that bug got lost in the 
  previous traversal work but that fix broke that 'edit top row/left 
  column' feature.

Intermediate release 4.51.01
-----------------------------
Yes, we've run out of numbers.

* Implement the proposed sorting API change for rows and columns and
  remove arcad's implementation. Documentation update.
* Fix some bugs related to 0 rows/columns.
* Make XbaeMatrixSetRow/ColumnLabels work when there were no labels previously.
  In the case of column labels one can now change the number of lines in a label.
* Fixes a few XmLabel issues.
* Fixes bugs related to unmanaging the text widget or not when cells are deleted.
* If an element of column_max_length is 0 it now means use the width of that column.
  I had to change the value of BAD_MAXLENGTH used in converters so it wouldn't conflict.
* This made me realize that my last change to allow NULL labels conflicts with what the
  converter uses as array terminator. I changed that to point to a char xbaeBadString.
* Reintroduced the checks on array bounds when resources tables are create by a converter.
  This was lost in the per cell move.
* Don't alloc an empty string for NULL labels, instead deal with NULL.
* Don't alloc per_cell systematically and fix a bug when it's not there
* Clean up add/delete rows/columns from table and remove a bunch of FIXMEs there
* Don't dereference function pointers explicitly and some indentation fixes.
* Simplified dynamic resizing a bit more.
* Cancel/commit edit only when absolutely necessary (when the cell being edit gets
  deleted, not for a bunch of SetValues cases or when deleting/adding rows/colums or
  dynamically resizing). Then get 'If no cells are being edited, then the upper left
  most visible non-fixed cell will be edited.' (from man page) to work.
* Set the TextWidget's XmNnaviagtionType to false at creation time. That way a
  XmProcessTraversal(TextChild(mw), XmTRAVERSE_NEXT_TAB_GROUP); will traverse out of the
  matrix and not to the clip child.  This allows us to kill matrix.traversal and what goes with
  it including 2 'yuck's and a 'sickening' in someones else's comments.
  It changes the default translations that are installed when the matrix is in a dialog
  so that the cancel key cancels the dialog. This should make the grace people happy.
  It also reworks the editcell method so that when a cell has a widget, it's the widget
  that gets the focus. If an app installs the correct translations on their user widgets,
  traversal works for these too.
* Fixes in the drawing of the resize indicators,
* Fixes problems with shadowTypes arrays that are too small in resourcees
* Fixes showColumnArrows issues
* Don't modify read only GCs returned by XtGetGC
* Utils.c Simplifications

Intermediate release 4.50.9901
------------------------------
* Fix for a crash caused by having no rows or columns.
* Fix formatting error in the documentation.
* Update the doc and doc/images directories so they have updated documentation.
  (That's the documentation in HTML format, which is the result of running
  groff on the documentation sources : src/Xbae*.in .)
* Remove a couple of lines of dead code.
* Add small example programs under examples/tests to try to figure out
  the bug reports about 4.50.99 .
* Fix XbaeMatrixSetCellWidget - some new code made it crash when the child widget
  was NULL.
* Get cell widgets to take the complete width and height of the cell, also take out
  some of the margins in the code positioning cell-widgets.
* More cleanup by Tobias :
  - Collapse addRowsToTable into addVarRowsToTable and makes addRows a wrapper
    for addVarRows.
  - Get roaming scroll/select to work along the new trackingACT. The scrolling
    speed is now proportional to the distance the mouse is away from the
    scrolled region.
  - Make getCellClip work for labels so redawRow/ColumLabel can use it
  - Introduce XmNnonfixedDetachedLeft and XmNnonFixedDetachedTop: when there
    are fixed rows/cols and XmNfill is set these new resources allow the
    filled space to be drawn between the fixed rows/cols and the non fixed
    ones. examples/choice lets you try it out. This patch also rearanges the
    funcions in Utils.c, making 2 of them static.
  - The attached patch gets rid of the private boolean row_heigths_used
    and introduces a new resource rowHeightsInPixel, that works the
    same way columnWidthInPixel does (except default is True).
    The symetry between rows and columns is getting pretty good!
* Bugfix for bug #1063271 - toggle buttons as cell widgets.
* Fix a problem reported by Bernd and Markus Sch�pflin - the definition of the
  widget ("fixed" recently) was wrong. It was lacking the "extern" keyword.
* (From Tobias)
  Remove a race in the traversal code and put the text_translations on the clip
  widget as well as the TextWidget. That way they work even when no cell is edited.
  (So one can traverse out of a matrix that has no editable cells). Now list and list2
  don't need to use undocumented stuff and work again. (bug 598708 examples/list/list2)
  I think Danny's sugestion for Michel's focus troubles should now also work.
* Tobias: Traversal of matrices with widgets. The user still has to bind the proper 
  translations on the widgets. Traverse out of the matrix isn't implemented.
* Tobias: Made sorting working and sane. Add support for sorting columns. See doc for
  more info.
Intermediate release 4.50.99
----------------------------
* Changed the configuration file for the build. The auto(conf|make) people
  changed their API so we changed our build.
* The string-to-pixel-table converter had gone missing; fixed.
* Several resources were not getting treated in SetValues and GetValues.
  Some must have been disabled for a while, several others died because
  moving them into the per-cell structure had the side-effect that they
  weren't any regular widget resources any more.
  I'm afraid this was oversight on my behalf during the per-cell conversion.
* Changed the functionality of drawCellCallback slightly: we're now initializing
  the callback structure with the cell contents. If the callback doesn't
  alter anything, something will still end up getting displayed.
* New work from Tobias Oed :
  - setlabel.patch corrects the buggy logic of XbaeSetRowLabel (and ColumnLabel).
  - SCROLLBAR_TOP.patch introduces two simple macros to make things a bit more readable
  - clips.patch redraws the left/right/bottom/top clips in the same way the clipchild is
    redrawn. IMHO it makes things cleaner and avoids redrawing the partially
    fixed cells twice in some cases. Also fixes bugs when deleting a row/col
    makes the matrix scroll.
  - ClearCell.patch fixes a small bug I introduced in GetCellClip.patch (one of the draw
    examples crashes).
  - deadspace-clipmask.patch redraws the dead space similarly to the labels (they are
    both drawn on the matrix). This makes things cleaner as now everything gets redrawn
    the way clipped cells are or the way labels are. It also eliminates the need to redraw
    the last non fixed row/columns to get the dead space to be drawn. It kills
    calcVertfill/calcHorzfill and removes a bunch of unnecessary 
  - xbaeSetClipMask calls
    and simplifies that function. It gets the shadow to span the dead space (when there are
    trailing attached rows/columns) and  one of XmGRID_ROW_SHADOW etc are set.
    It simplifies the way a highlights are drawn and deprecates HighLightOther but these
    do not span the dead space yet.
* Protect against passing gadgets as cell widgets for now, because we don't have the
  right support for them yet.
* More work from Tobias :
  - refresh.patch adds stuff I missed in GetCellClip.patch that leads to this bug: start
    choice, add some (trailing)fixed columns, set cell margin heigtht to 10, scroll to the 
    bottom of the matrix and then reduce the margin height. The fixed column don't get
    updated right.
  - left_column.patch. Do the same thing with the above patch applied.  Everything gets
    updated correctly now, but the scrolling is somewhat erratic. This patch fixes this by
    making left_column/top_row 'fake' resources (like cells now is). It relies on the assumption
    that SetValuesHook gets called after SetValues. If someone has a better X doc than me, let
    me know if this is safe.
  - filletc.patch gets the fill(s)/trailing attached to work under all combinations of these
    options and shadow types according to spec (these are wierd). The approach I started 
    to take in deadspace-clipmask.patch was a hopless nightmare and this is much simpler. It makes
    the fill part of the cell that preced it. To accomodate this, the size of the clips is changed.
    It removes the more obscure macros (Sir Silly Macro's) and introduces new ones
    VISIBLE_FIXED_*_{HEIGHT|WIDTH}. The new version of exaples/add lets you try the stuff out.
  - renamemacro.patch does the renaming I suggested.
  - rowlabelwidth.patch fixes the calculation of the row label width as it included
    cell_shadow_thickness twice when the labels were buttons.
  - labelClip.patch - This patch puts the scrollable parts of labels in their own clips. It adds 
    two clips and two scrollMgrs but on the other hand kills 3 gcs and the matrix scrollMgr.
    Now everything that scrolls is treated the same way and we have as many scrollMgrs as clips.
    Time to put them in the clip.
  - This patch fixes a few more problems due to the per_cell change of arcad: Set/GetValuesHook
    were missing some items and SetValuesHook wasn't updating the display.
    I also changes the semantics of per_cell[][].shadow_type a bit: a value of 0 now
    means to use the default matrix.cell_shadow_type. Unfortunately there is no
    XmUNSPECIFIED_SHODOW so this may not work on all motif flaviours. On
    the other hand, the values currently used come from two different motif enums
    so it's either already broken or safe.This gets that part of examples/choice to work again.
    I modified examples/choice to get the Arm option to work again (You can't just
    fiddle with what GetValues returned for cellBackgrounds and cellShadowTypes
    and do a redisplay, you need to SetValues what you changed). I also fixed the
    memory leaks that every other third party app will suffer after a GetValues of
    these resources. That probably needs to be documented somewhere.
  - fifteen.patch gets the shadow to follow the empty cell again and fixes some memory leaks
    (same thing as my latest changes to choice.c, all other examples are OK)
  - labelClipfix.patch fixes a bug I introduced in labelClip.patch (very apparent when scrollbars
    are TOP and/or LEFT). I had to remove some XClearArea calls from resize() . I had doubts about
    them for a while: they were using outdated values. I also decided to have labels clear their
    'cell' before redrawing and collapsed drawXmLlabel into drawLabel.
  - ClearArea breaks relayout out of resize and removes all calls to
    XClearArea from these functions, adding them to add/delete row/column.
    This hopefully gets all that working. It also eliminates private header
    dependence of some examples allowing to remove xbaeClearCell.
  - CopyPerCell.patch replaces calls to xbaeCopyPerCell by xbaeCreatePerCell
    where apropriate. After that there is a call to copypercell left in a dead
    piece of code and another one that is commented out. So I killed the dead
    code and the whloe xbaeCopyPerCell function.
  - userWidget.patch: (at least) since the new XbaeMatrixSetUserWidget apeared,
    user widgets can be treated just like the text widget and xbaeMoveUserWidget
    is not needed anymore.
* Fixes from Michel Bardiaux :
  - allow build using an older gcc (still the official compiler in Debian)
  - fix a 'BadWindow' happening with the following sequence:
	XbaeCreateMatrix
	XtRealizeWidget(toplevel)
	create a widget as child of matrix
	XbaeMatrixSetCellWidget
* Another one from Tobias : The first part makes the scroll managers part of the clips,
  and the second part isolates all the scrolling to the clips widgets.
* Change the signature of XbaeMatrixGetCellPixmap so it has a return code.
* Implement XbaeMatrixSortRows() as the same function as XbaeMatrixSort().
  Maybe the latter should disappear.
* Document the new interfaces from Arcad.
* Include code for creating a DLL under the Exceed environment.
  Until we figure out how to auto-detect Exceed, a flag should be passed to configure
  to enable this.

Intermediate release 4.50.97
----------------------------
* A submission of code by Arcad Systemhaus for :
  - Added XbaeMatrixSort(Widget, void *proc);
  - Moved the cell string into the per_cell structure, to be able to sort.
  - Fixes for multiline text display in cells.
  - Added XbaeMatrixTrackCellCallback which fires everytime the cursor enters
    a new cell (something entercell/leavecell do differently).
* Modified ("fixed" really) cell width calculation, as requested by Evgeny,
  to mimic the behaviour described in the Motif documentation.

Test release 4.50.93
--------------------
* Lots of bugfixes and code cleanup by Tobias Oed, see ChangeLog.

Test release 4.50.91
--------------------
* Lots of bugfixes and code cleanup by Tobias Oed, see ChangeLog.

Release 4.50.5
--------------
* Fix for XmNautoFill in XbaeInput when the pattern begins with an
  optional literal, such as [-]d[d][d][d][d] .
* Add the new resources introduced in the last months to the example
  Builder Xcessory integration file in examples/builderXcessory.
* Bugfix for a clip window size problem that showed up when resizing the
  window (e.g. resize examples/traversal/traversal to smaller than the
  original and then bigger). Bug #702560.
* Some source code cleanup.
* Fixes by James Georgas for colour handling.
* Fixes by Van to eliminate slider size warnings (see bugs #823041 and #823037).
* Fixes by David Traill for both resize and scrollbar warnings.
* Add XmNXmColumnLabel resource to handle XmString as column labels.
* Fix a memory problem with row_heights.

4.50.3 was not an official release.
	It was a test version distributed to parties interested in it.

Release 4.50.2
--------------
* A memory (double free) related bugfix.
* Two new resources (XmNhorzFill, XmNvertFill) were added to allow for 
  additional fill behaviour. This allows you to specify how the matrix
  treats highlighting of the last row or column.
* The foreground and background resources are no longer being set on
  cell widgets, as this appears to be strange behaviour.
* Bugfix related to incorrect refresh after deleting rows.
* Remove some C++ style comments.
* Fix some build problems.
* Move some stuff in the share/ directory at installation.
* Implement the XBAE_PRODUCTION symbol to compile Xbae with or without
  debugging code.
* Improve resizing rows and columns.
* Avoid X Errors when setting clip geometry to 0.
* Implement showColumnArrows and showRowArrows resources.
* Fix incorrect behaviour when rapidly clicking with two different
  mouse buttons.
* Fix nested comments and a missing prototype.
* Fix scrollbar warning problem.
* Changed the column width measurement.

Release 4.50
------------
This is a stable release, basically equivalent to 4.9.13.

Changes in 4.9.13
-----------------
* Fix the GROHTML build problem.
* Bugfix : when resizing first of trailing fixed rows, the VSB slider
  was not updated and a blank area was visible between clip and bottomclip.
* Changed useXbaeInput default value to False.

Changes in 4.9.11
-----------------
* Sascha G�bel and Greg Shebert are now part of the Xbae development team.
* Many bugfixes to the scrolling (smooth scrolling now),
  and to resizing row heights.
* Received code from Sasha G�bel.

Changes in 4.9.9
----------------
* Bug fixes related to redrawing.
* Enable dynamic row/column resizes again.
* Include images in release files again - this makes for a large package.
* Build HTML manual pages from their sources (the HTML is in doc/,
  but the sources are in src/*.3.in).

Changes in 4.9.7
----------------
* Sascha G�bel contributed substantial improvement to scrolling.
* Bugfixes.

Changes in 4.9.5
----------------

* Improved internal debugging support (dmalloc, dbmalloc)
* Fix pattern check in XbaeInput
* Remove support for Motif <=1.1
* Bugfix by Phil Eccles to position cell widgets correctly when row/column
  headers are active.


Changes in 4.9.1
----------------

Xbae now stand-alone project on sf.net:
  http://xbae.sf.net

  
Changes in 4.9.0
----------------
* Row height and true-color (aka 24-bit color) support are now considered stable
  and enabled by default.
* Internals : try to give all non-static functions an Xbae prefix.
* Code contributed by Frank Mair for having bold columns in XbaeMatrix.
* Bugfixes.


Changes in 4.8.x
----------------

Xbae is now maintained by the LessTif Development Team
(see http://www.lesstif.org)

A detailed list of changes to the library can be found in
the ChangeLog file.

Comments, questions, bugs & postcards can be directed to the
LessTif mailing list <lesstif@lesstif.org> .

* Resizable rows

* TrueColor support


Changes in 4.7.2
----------------
* CELL_WIDGETS fixes by Linas Vesptas.

* CELL_WIDGETS functionality extended with XbaeMatrixGetCellWidget()
  which you can use to figure out whether a cell already has a
  widget.

* CELL_WIDGETS now enabled by default, and somewhat implemented.


Changes in 4.7.1
----------------
* Maintenance is transferred to the LessTif core team.

* Configuration based on automake, autoconf, libtool is added, this
  is the preferred environment for the current (new) maintainers.


Changes in 4.7
----------------

* Swapped out the XmTextField widget to use the XmText widget to enable
  multi line rows and the like - thanks to Mark Gibson (magibso@uswest.com)
  for his methods in achieving this goal.

* Added a patch from Philip Aston (philipa@parallax.co.uk) that
  reportedly speeds up things by 300% when using pixmaps.  Acheivable
  by specifying the width, height and depth of a pixmap to an
  XbaeMatrixDrawCellCallbackStruct.

* Another patch from Philip provided correct behaviour when setting
  the XmNsensitive resource on ancestors of the matrix widget.

* "Ceklosky" (b1dqza9@is200005.bell-atl.com) reported a memory leak
  when the widget was destroyed.  This came about from highlighted-
  cells not being freed.

* Donato Petrino (dp@rtsffm.com) provided an efficient way of detecting
  if partially displayed rows were visible

* He also supplied a patch that corrected the placement of the text
  widget if the user first clicked on an editable fixed row.

* Scott Harrington (scotth@ibl.bm) figured out that xbaeComputeSize()
  should not add space for scrollbars if the displayPolicy is set to
  XmDISPLAY_NONE.  He also provided a patch.

* Made a small change to a conditional in Matrix.c that prevents
  an erroneous warning message being displayed.  Thanks to Curt
  Malouin (cmalouin@ford.com) for that one.

* Merged two patches from Jay Schmidgall (Jay.Schmidgall@Sungardss.com)
  one fixed a slight error in the drawing of scrollbar heights/widths
  while the other fixed some casting problems under Solaris [they
  must be getting stricter on this ANSI C thing!]

* Brian McAllister (brian@hotrats.mit.edu) provided a fix for compilation
  of Xbae on Dec Alphas

* Jay Schmidgall continued work on the widget, providing:

o Corrected some bugs with row/column shadow drawing and clearing
o Corrected some bugs related to not having row/column labels
o Changed the way shadows are drawn to be the correct way,
  i.e., mine. That is, XmSHADOW_OUT results in an outset shadow.
  Note: this may have implications for Motif 1.1 compatibility,
  since type isn't passed along to _XmDrawShadow()
(ED: Since 1.1 is no longer supported, this isn't an issue!)
o Changed the grid type enumeration to be more explicit
o Made the unneeded grid types deprecated, which will result
  in runtime warning messages
o Added grid types XmGRID_ROW_LINE and XmGRID_COLUMN_LINE
  (this was suprisingly simple :)
o Modified the matrix resource declaration to come into line
  with grid/shadow changes
o Modified the choice example program to reflect these changes
o Modified the man page to reflect these changes

*** NOTE ************************************************************
  The new types for gridType are:
  enum
  {
      XmGRID_NONE             = 0x00,
      XmGRID_CELL_LINE        = 0x02,
      XmGRID_CELL_SHADOW      = 0x03,
      XmGRID_ROW_LINE         = 0x04,
      XmGRID_ROW_SHADOW       = 0x05,
      XmGRID_COLUMN_LINE      = 0x08,
      XmGRID_COLUMN_SHADOW    = 0x09,

      /* Deprecated types. Use will cause
       * a run-time warning to be issued. */
      XmGRID_LINE             = 0x20,
      XmGRID_SHADOW_IN        = 0x40,
      XmGRID_SHADOW_OUT       = 0x80
  };

  If any of the bottom three values, a runtime warning will be output.
  You simply need to tweak the resources in your app to avoid it.

*********************************************************************

* Added a patch supplied by Martin Helmling (mh@octogon.de) that
  correctly manages the insensitive pixmap when creating for multiple
  screens.  Patches were also sent in by Dimitri Castiglioni and
  Donato Petrino.

* Added the doc/XbaeMatrix.man.html file as part of the official release

* Jay Schmidgall sent in another patch!  This time he:

o got irritated by how the matrix did column shadows/lines.
  In particular, there could be a little gap between the last
  non-fixed row and the first trailing fixed row, and that gap
  looked bad because the column shadow just ended there. So,
  I modified the shadow & highlight code so that gap got filled
  in. Looks much better now.
o Fixed the column resize shadow so it draws all the way to the
  bottom of the matrix, plus handles the gap like the above.
o Added a new resource, XmNtrailingAttachedBottom. (If you can
  come up with a better name, feel free :). This resource makes
  the matrix keep the trailing fixed rows on the bottom. It only
  has effect when XmNfill is true and there are trailing fixed
  rows. It's probably easier to understand visually.
o I put the equivalent AttachedRight in the matrix's resources,
  but I didn't feel up to doing the code for it :).
o Added a bunch of macros.
o Added the resource to the man page (yay!)
o Mucked around some more with the choice program to add the
  attachedBottom resource, and let you change the selection mode.
  I did this mostly because choice is the test bed for changes I
  make, and I needed to test the highlighting. Also let you do
  Arm mode, which looks neat in the grid shadow modes.

* Philip Aston sent in yet another patch that remedied an array
  bounds read as reported by Purify.

* A segmentation fault occurred if the user pressed <ESC> when no
  cells had been defined.  Diligently spotted and patch provided
  by Noel Yap (nyap@garban.com)

* Removed an unnecessary reference to XmNeditVerifyCallback in
  Matrix.h that Noel found as well.

* Added fix from Jon Sibala (jon_sibala@hotmail.com) where zero 
  rows was causing a crash in xbaeDrawCell.

* David North reported a bug in the charWidth macro on OpenVMS.
  A cast to unsigned char of the passed character was required.

* Marty Leisner (leisner@sdsp.mc.xerox.com) advised that the runtest
  script required '.' to be in your path.  Added an explicit ./ to
  all commands run from the script

* Cleaned up a few potential problems with compilation

* Removed the xqCellWidget from the release. The subclassed widget
  started causing problems when using Motif 2.1 probably due to
  incompatibilities between the XmText and XmTextField widgets
  The change may also fix problems reported by the guys back at
  Bellcore, running under Motif 2.1

* There are some new public functions:- XbaeMatrixSetRowLabel and,
  funnily enough, XbaeMatrixSetColumnLabel and their Get equivalents
  are now documented in the man page.

* Just as some sort of benchmark, Callum and myself loaded up a
  historical database comprising of somewhere around 780,000 rows and
  3 columns.  Using the drawCellCallback, XbaeMatrix handled display
  of the data as if there were 100 rows.  If anyone has tried creating
  a bigger table, please let me know the results!  (That applies to
  tables created with commercial widgets too)

* The building of shared libraries is now the default behaviour in
  the Imakefile.  If anybody has trouble running the demos due
  to the shared library not being picked up, please let me know
  so we can work out a fix.

* The HTML man page is probably a bit behind as I prefer methods
  from yesteryear.  Updates are always appreciated!

* Removed a strdup call and added __EMX__ to the preprocessor directives
  for not having bcopy.  Patch sent in by Evgeny Stambulchik
  (fnevgeny@plasma-gate.weizmann.ac.il)

* Two fixes from Mark Crowther (markc@parallax.co.uk) that better
  controlled scrolling with the pointer outside the clip and prevented
  the scrollbar warnings when columns were deleted.

* Noel Yap (nyap@garban.com) pointed out a small error in the draw
  example where the columns for pixmaps was incorrect

* Added the XEvent structure to all callbacks

* New widget!  XbaeInput allows a 'pattern' to be specified to restrict
  input to certain characters.  Read the new man page to find out
  more.

* Added some extra parameters to the XbaeMatrixEnterCellCallbackStruct -
  namely, position, pattern, auto_fill, convert_case, and
  overwrite_mode for use with the XbaeInput widget.
  Also, the params and num_params are passed through to the
  enterCellCB too.

* New resource - XmNcalcCursorPosition that allows the pointer to
  specify the insert position when clicked on a cell to be edited
  rather than at the end (default is False so behaviour is unchanged)

* Nick Banyard (nbanyard@micromuse.com) provided the ability to
  allow multibyte characters to be displayed with the addition of an
  XFontSet.  There is a new example program also, named multibyte
  that *should* display multibyte characters but some work may be
  needed there.

* Mark Hatch (mhatch@ics.com) provided me a free copy of Integrated
  Computer Solutions (http://www.ics.com) Builder Xcessory for both
  Solaris and Linux so the integration and compatibility between the
  two can be maintained.  If you're looking for an excellent GUI builder,
  I'd recommend checking this one out!

* Added a matrix creation convenience routine - XbaeCreateMatrix for
  smoother integration into Builder Xcessory

* Added a caption creation convenience routine - XbaeCreateCaption for
  smoother integration into Builder Xcessory

* Added an input creation convenience routine - XbaeCreateInput for
  smoother integration into Builder Xcessory

* Evgeny Stambulchik (fnevgeny@plasma-gate.weizmann.ac.il) supplied the
  GNU autoconf tools needed to compile the widget via ./configure as so
  many people feel the urge to do nowadays.  However, I have no intention
  of supporting it so you'd better have a friend who can & will.

* Callum Gibson (callum.gibson@aus.deuba.com) wrote a couple of new
  public functions to refresh entire rows or columns efficiently.
  They are called XbaeMatrixRefreshRow() and XbaeMatrixRefreshColumn().

* The XbaeMatrixAnyCallbackStruct was created to allow the reason, 
  event, row and column members to be aligned and facilitate the use
  of one callback struct in multiple callbacks.

* Fixed a bug with wrong fg/bg colours when editing a cell with
  reverse select enabled and using a draw cell callback which set
  foreground or background. (Everyone does that don't they?)

* Added a new action that allows the matrix to be scrolled up and
  down using the PgUp/PgDn keys when the text field is mapped.  It
  is installed by default.
