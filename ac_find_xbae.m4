dnl
dnl
dnl AC_FIND_XBAE : find libXbae, and provide variables
dnl	to easily use them in a Makefile.
dnl
dnl Adapted from a macro by Andreas Zeller.
dnl
dnl The variables provided are :
dnl	link_xbae		(e.g. -L/usr/lesstif/lib -lXm)
dnl	include_xbae		(e.g. -I/usr/lesstif/lib)
dnl	xbae_libraries		(e.g. /usr/lesstif/lib)
dnl	xbae_includes		(e.g. /usr/lesstif/include)
dnl
dnl The link_xbae and include_xbae variables should be fit to put on
dnl your application's link line in your Makefile.
dnl
AC_DEFUN([AC_FIND_XBAE],
[
AC_REQUIRE([AC_FIND_MOTIF])
xbae_includes=
xbae_libraries=
AC_ARG_WITH(xbae,
[  --without-xbae         do not use Xbae widgets])
dnl Treat --without-xbae like
dnl --without-xbae-includes --without-xbae-libraries.
if test "$with_xbae" = "no"
then
    xbae_includes=no
    xbae_libraries=no
fi
AC_ARG_WITH(xbae-includes,
    [  --with-xbae-includes=DIR    Xbae include files are in DIR], xbae_includes="$withval")
AC_ARG_WITH(xbae-libraries,
    [  --with-xbae-libraries=DIR   Xbae libraries are in DIR], xbae_libraries="$withval")
if test "$xbae_includes" = "no" && test "$xbae_libraries" = "no"
then
    with_xbae="no"
fi

AC_MSG_CHECKING([for Xbae])
if test "$with_xbae" != "no"
then
    #
    #
    # Search the include files.
    #
    if test "$xbae_includes" = ""
    then
	AC_CACHE_VAL(ac_cv_xbae_includes,
	[
	ac_xbae_save_CFLAGS="$CFLAGS"
	ac_xbae_save_CPPFLAGS="$CPPFLAGS"
	#
	CFLAGS="$MOTIF_CFLAGS $X_CFLAGS $CFLAGS"
	CPPFLAGS="$MOTIF_CFLAGS $X_CFLAGS $CPPFLAGS"
	#
	AC_TRY_COMPILE([#include <Xbae/Matrix.h>],[int a;],
	[
	# Xbae/Matrix.h is in the standard search path.
	ac_cv_xbae_includes=
	],
	[
	# Xbae/Matrix.h is not in the standard search path.
	# Locate it and put its directory in `xbae_includes'
	#
	# Other directories are just guesses.
	for dir in "$x_includes" "${prefix}/include" /usr/include /usr/local/include \
		   /usr/include/Motif2.0 /usr/include/Motif1.2 /usr/include/Motif1.1 \
		   /usr/include/X11R6 /usr/include/X11R5 /usr/include/X11R4 \
		   /usr/dt/include /usr/openwin/include \
		   /usr/dt/*/include /opt/*/include /usr/include/Xbae* \
		   "${prefix}"/*/include /usr/*/include /usr/local/*/include \
		   "${prefix}"/include/* /usr/include/* /usr/local/include/* \
		   "${HOME}"/include
	do
	    if test -f "$dir/Xbae/Matrix.h"
	    then
		ac_cv_xbae_includes="$dir"
		break
	    fi
	done
	])
	#
	CFLAGS="$ac_xbae_save_CFLAGS"
	CPPFLAGS="$ac_xbae_save_CPPFLAGS"
	])
	xbae_includes="$ac_cv_xbae_includes"
    fi

    if test -z "$xbae_includes"
    then
	xbae_includes_result="default path"
	XBAE_CFLAGS=""
    else
	if test "$xbae_includes" = "no"
	then
	    xbae_includes_result="told not to use them"
	    XBAE_CFLAGS=""
	else
	    xbae_includes_result="$xbae_includes"
	    XBAE_CFLAGS="-I$xbae_includes"
	fi
    fi
    #
    #
    # Now for the libraries.
    #
    if test "$xbae_libraries" = ""
    then
	AC_CACHE_VAL(ac_cv_xbae_libraries,
	[
	ac_xbae_save_LIBS="$LIBS"
	ac_xbae_save_CFLAGS="$CFLAGS"
	ac_xbae_save_CPPFLAGS="$CPPFLAGS"
	#
	LIBS="-lXbae -lm $MOTIF_LIBS $X_LIBS $X_PRE_LIBS -lXt -lX11 $X_EXTRA_LIBS $LIBS"
	CFLAGS="$XBAE_CFLAGS $MOTIF_CFLAGS $X_CFLAGS $CFLAGS"
	CPPFLAGS="$XBAE_CFLAGS $MOTIF_CFLAGS $X_CFLAGS $CPPFLAGS"
	#
	AC_TRY_LINK([#include <Xbae/Matrix.h>],[Widget w; XbaeMatrixDeselectAll(w);],
	[
	# libXbae.a is in the standard search path.
	ac_cv_xbae_libraries=
	],
	[
	# libXbae.a is not in the standard search path.
	# Locate it and put its directory in `xbae_libraries'
	#
	# Other directories are just guesses.
	for dir in "$x_libraries" "${prefix}/lib" /usr/lib /usr/local/lib \
		   /usr/lib/Xbae \
		   /usr/lib/X11R6 /usr/lib/X11R5 /usr/lib/X11R4 /usr/lib/X11 \
		   /usr/dt/lib /usr/openwin/lib \
		   /usr/dt/*/lib /opt/*/lib /usr/lib/Xbae* \
		   /usr/lesstif*/lib /usr/lib/Lesstif* \
		   "${prefix}"/*/lib /usr/*/lib /usr/local/*/lib \
		   "${prefix}"/lib/* /usr/lib/* /usr/local/lib/* \
		   "${HOME}"/lib
	do
	    for ext in "sl" "so" "a" "lib"; do
		if test -d "$dir" && test -f "$dir/libXbae.$ext"; then
		    ac_cv_xbae_libraries="$dir"
		    break 2
		fi
	    done
	done
	])
	#
	LIBS="$ac_xbae_save_LIBS"
	CFLAGS="$ac_xbae_save_CFLAGS"
	CPPFLAGS="$ac_xbae_save_CPPFLAGS"
	])
	#
	xbae_libraries="$ac_cv_xbae_libraries"
    fi
    if test -z "$xbae_libraries"
    then
	xbae_libraries_result="default path"
	XBAE_LIBS="-lXbae"
    else
	if test "$xbae_libraries" = "no"
	then
	    xbae_libraries_result="told not to use it"
	    XBAE_LIBS=""
	else
	    xbae_libraries_result="$xbae_libraries"
	    XBAE_LIBS="-L$xbae_libraries -lXbae"
	fi
    fi
#
# Make sure, whatever we found out, we can link.
#
    ac_xbae_save_LIBS="$LIBS"
    ac_xbae_save_CFLAGS="$CFLAGS"
    ac_xbae_save_CPPFLAGS="$CPPFLAGS"
    #
    LIBS="$XBAE_LIBS -lm $MOTIF_LIBS $X_LIBS $X_PRE_LIBS -lXt -lX11 $X_EXTRA_LIBS $LIBS"
    CFLAGS="$XBAE_CFLAGS $MOTIF_CFLAGS $X_CFLAGS $CFLAGS"
    CPPFLAGS="$XBAE_CFLAGS $MOTIF_CFLAGS $X_CFLAGS $CPPFLAGS"

    AC_TRY_LINK([#include <Xbae/Matrix.h>],[Widget w; XbaeMatrixDeselectAll(w);],
	[
	#
	# link passed
	#
	AC_DEFINE(HAVE_XBAE)
	],
	[
	#
	# link failed
	#
	xbae_libraries_result="test link failed"
	xbae_includes_result="test link failed"
	with_xbae="no"
	XBAE_CFLAGS=""
	XBAE_LIBS=""
	]) dnl AC_TRY_LINK

    LIBS="$ac_xbae_save_LIBS"
    CFLAGS="$ac_xbae_save_CFLAGS"
    CPPFLAGS="$ac_xbae_save_CPPFLAGS"
else
    xbae_libraries_result="told not to use it"
    xbae_includes_result="told not to use them"
    XBAE_CFLAGS=""
    XBAE_LIBS=""
fi
AC_MSG_RESULT([libraries $xbae_libraries_result, headers $xbae_includes_result])
AC_SUBST(XBAE_CFLAGS)
AC_SUBST(XBAE_LIBS)
])dnl AC_DEFN
